# encoding = utf-8
__author__ = 'mochenx'

import re


class DocxToken(object):
    _me = None
    @staticmethod
    def sub_re_with_token(text, re_expr, func_token_builder):
        """
        Arguments:
            text: String, the one being scaned
            re_expr: Regular expression
            func_token_builder: Builder function, return an object of DocToken or its subclass and the remain index
        Return:
            a list of strings
        """
        compiled_re_expr = re.compile(re_expr)

        splited_txt_lst = []
        remain_idx = 0
        for m_rslt in compiled_re_expr.finditer(text):
            # Call object builder
            before_str, new_token, remain_idx = func_token_builder(text, remain_idx, m_rslt)

            if not isinstance(new_token, DocxToken):
                raise TypeError('The builder function MUST return a object of DocxToken or its subclass')
            if remain_idx > len(text):
                raise ValueError('The remain index builder function returned MUST be less than total length of text')

            if before_str:
                splited_txt_lst.append(before_str)
            splited_txt_lst.append(new_token)
        if remain_idx < len(text) and not re.match(r'^\s+$', text[remain_idx:]):
            splited_txt_lst.append(text[remain_idx:])

        return splited_txt_lst

    @classmethod
    def get(cls, *args, force_new=False, **kwargs):
        if force_new:
            return cls(*args, **kwargs)
        elif cls._me is None:
            cls._me = cls(*args, **kwargs)
        return cls._me

