# encoding=utf-8
__author__ = 'mochenx'

"""
A label is a word, a phrase or a short sentence, which resides on the left side in an assignment,
or as a column title in table.
It works like a variable type, denotes that the right side value in an assignment
or the contents in below cells belong to the a concept.
"""

from RegDocxParser.DocxToken import DocxToken

import nltk
import nltk.data


class Label(DocxToken):
    """
        Base class of all classes which are instantiated in a large number times, but most of the instances store same
        information. Concerning that, we call the same information as a 'name' of instance. Then, only different named
        instances will be created for saving memory.
    """
    _all_names = {}

    @staticmethod
    def is_valid_label_types(*labels):
        for label in labels:
            if not isinstance(label, str) and not isinstance(label, Label):
                raise TypeError('Only string and Label(and its subclass) object is supported '
                                'by Label as its name, {0} is not support'.format(type(label)))
        return True

    @classmethod
    def get(cls, name):
        """
            Return new instance if given name doesn't exist, or return the existed one.
        """
        if isinstance(name, tuple):
            assert Label.is_valid_label_types(*name)
        else:
            assert Label.is_valid_label_types(name)

        if name in cls._all_names:  # Name already exist
            gotten_type = cls._all_names[name]
        else:
            gotten_type = cls.create_with_name(name)
            cls._all_names[name] = gotten_type
        return gotten_type

    @classmethod
    def create_with_name(cls, name):
        raise NotImplementedError('Method of create_with_name must be implemented in subclass')

    @classmethod
    def get_all_names(cls):
        return list(cls._all_names.keys())

    @classmethod
    def clear(cls):
        cls._all_names.clear()


class MetaLabelCreator(type):
    """
        The Meta-class for creating subclass of Label, we can customize subclass dynamically.
    """
    def __new__(mcs, name, bases, class_dict):
        result = type.__new__(mcs, name, (Label, ), dict(class_dict))
        result._all_names = {}
        return result


class LabelFrequency:
    _all_names = {}

    @classmethod
    def get(cls, name):
        """
            Return new instance if given name doesn't exist, or return the existed one.
        """
        if name in cls._all_names:  # Name already exist
            gotten_type = cls._all_names[name]
        else:
            gotten_type = cls.create()
            cls._all_names[name] = gotten_type
        return gotten_type

    @classmethod
    def create(cls):
        return LabelFrequency()

    @classmethod
    def clear(cls):
        cls._all_names.clear()

    # TODO: a method to clear all_char
    def __init__(self):
        self._all_sequences = []
        self.phrases_with_freq = None
        self.sequence_changed = True

    def collect(self, sequence_of_words):
        if isinstance(sequence_of_words, list):
            self._all_sequences.extend(sequence_of_words)
        else:
            self._all_sequences.append(sequence_of_words)
        self.sequence_changed = True
    # TODO: Methods of saving & loading phrase DB

    def filter_invalid_label(self, key=None):
        """
            Filter number components in given phrases, and the frequency is untouched
        """
        if key is not None:
            is_invalid_phrase = key
        else:
            is_invalid_phrase = lambda e: False
            # lambda e: self.re_num_patn.match(e[0]) or self.re_num_patn.match(e[1])

        valid_phrases = []
        for phrase, freq in self.phrases_with_freq.items():
            if is_invalid_phrase(phrase):
                continue
            valid_phrases.append((phrase, freq))
        return valid_phrases

    def get_partitions(self, invalid_key=None):
        """
            Calculate the frequency of all bigrams, and select the most common 1% as phrases
        """
        if self.sequence_changed and len(self._all_sequences) > 0:
            self.phrases_with_freq = nltk.FreqDist(self._all_sequences)
            self.sequence_changed = False

        if self.phrases_with_freq is not None:
            # Phrase is formed as 'word word', number is not allowed in it
            valid_phrases_with_freq = self.filter_invalid_label(invalid_key)

            phrase_partitions = LabelPartition.build_from_freq(valid_phrases_with_freq)
        else:
            raise ValueError('No sequence of words is found. '
                             'Please call method "collect" before calling get_partitions')

        return phrase_partitions

    @property
    def sample_size(self):
        if self.sequence_changed and len(self._all_sequences) > 0:
            self.phrases_with_freq = nltk.FreqDist(self._all_sequences)
            self.sequence_changed = False

        if self.phrases_with_freq is None:
            sample_size = 0
        else:
            sample_size = self.phrases_with_freq.N()
        return sample_size


class LabelPartition(list):
    """
        Partition list of labels according to the differences of their frequency. For example:
            [('Text1', 100), ('Text2', 95), ('Text3', 60), ('Text4', 50), ('Text5', 4), ('Text6', 4), ('Text7', 1)]
        will be partitioned to
            [[('Text1', 100), ('Text2', 95)],
            [('Text3', 60), ('Text4', 50)],
            [('Text5', 4), ('Text6', 4), ('Text7', 1)]]
    """

    @classmethod
    def build_from_freq(cls, elem_freq_list):
        new_partition = cls()
        new_partition.partition_list_in_freq(elem_freq_list)
        return new_partition

    def partition_list_in_freq(self, elem_freq_list):
        """
            Partition label list according to the average of differences between two near labels.
            For difference which is less than the average, group them together.
            Whenever a larger difference occurs, start a new group.
        """
        get_freq = lambda e: e[1]
        get_diff_of_near_label = lambda e: [get_freq(e[i+1])-get_freq(e[i]) for i in range(len(e)-1)]

        self.clear()
        elem_freq_ascend = sorted(elem_freq_list, key=get_freq)
        freq_diff = get_diff_of_near_label(elem_freq_ascend)

        # The same differences are ignored when computing average difference
        num_of_diff = len([d for d in freq_diff if d > 0])
        avg_diff = sum(freq_diff) / num_of_diff

        self.append_partition_with(elem_freq_ascend[0])
        for label, freq in elem_freq_ascend[1:]:
            if freq - self.freq_of(self.last_elem) < avg_diff:
                self.last_partition.append((label, freq))
            else:  # Whenever a larger difference occurs, start a new group.
                self.append_partition_with((label, freq))

    def append_partition_with(self, elem):
        # new_partition =
        self.append([elem])

    def of_freq_greater(self, than=None, and_equal_to=None):
        choosed_partition = []
        for partition in reversed(self):
            # For the reason that the frequency of element in a partition(Referred as 'the frequency' in the following)
            #   is sorted ascend, if equal condition is not included, it should be judged firstly.
            if than is None and and_equal_to is None:
                raise ValueError('Both "than" and "and_equal_to" are unset in method "of_freq_greater"')
            elif than is not None and self.freq_of(partition[-1]) <= than:
                break
            elif and_equal_to is not None and self.freq_of(partition[-1]) < and_equal_to:
                break
            choosed_partition.extend(partition)
        return choosed_partition

    def of_freq_less(self, than=None, and_equal_to=None):
        choosed_partition = []
        for partition in self:
            # For the reason that the frequency of element in a partition(Referred as 'the frequency' in the following)
            #   is sorted ascend, if equal condition is not included, it should be judged firstly.
            if than is None and and_equal_to is None:
                raise ValueError('Both "than" and "and_equal_to" are unset in method "of_freq_less"')
            elif than is not None and self.freq_of(partition[0]) >= than:
                break
            elif and_equal_to is not None and self.freq_of(partition[0]) > and_equal_to:
                break
            choosed_partition.extend(partition)
        return choosed_partition

    @property
    def last_partition(self):
        return self[-1] if len(self) > 0 else None

    @property
    def last_elem(self):
        return self[-1][-1] if len(self) > 0 and len(self[-1]) > 0 else None

    @staticmethod
    def freq_of(elem):
        return elem[1]
