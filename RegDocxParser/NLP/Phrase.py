# encoding=utf-8
from RegDocxParser.Stream.TextStream import TextStreamCursor

__author__ = 'mochenx'

import re
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.NLP.Label import MetaLabelCreator, LabelFrequency
from RegDocxParser.Tokens.Category import Category
from RegDocxParser.BuiltinRules.RuleService import RuleServiceVarName


class Phrase(metaclass=MetaLabelCreator):
    """
        In this project, a phrase is composed from two words, we call them word0 & word1.
    """
    def __init__(self, phrase):
        self.phrase = phrase
        self._token = None

    @classmethod
    def create_with_name(cls, phrase):
        return Phrase(phrase)

    # TODO: Implement get method of this class
    def __str__(self):
        return self.phrase

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, token_or_its_name):
        if isinstance(token_or_its_name, str):
            self._token = Category.get(name=token_or_its_name)
        elif isinstance(token_or_its_name, Category):
            self._token = token_or_its_name

    @property
    def name_number(self):
        return len(self._all_names)


class SlicingInStream:
    """
        SlicingInStream works like the slicing built in python, the left bound is included and the right is EXCLUDED
    """
    def __init__(self, word):
        self.word = word
        self.re_word = re.compile(word, flags=re.IGNORECASE)
        self.match_idx = None
        self.match_start_pos = None
        self.match_end_pos = None

    def find_word(self, stream, start_cursor):
        """
            set values to the start_index, start_position and end_position of matched word
        """
        if not isinstance(start_cursor, TextStreamCursor):
            raise TypeError('Argument "start_cursor" of method "find_word" should be type TextStreamCursor '
                            'instead of {0!s}'.format(type(start_cursor)))
        _matched = False
        for i, t in enumerate(stream):
            if i < start_cursor.index:
                continue
            elif _matched:
                break
            elif isinstance(t, DocxToken):
                continue

            for m_rslt in self.re_word.finditer(t):
                self.match_start_pos = m_rslt.start(0)
                if i == start_cursor.index and self.match_start_pos < start_cursor.position:
                    continue
                self.match_idx = i
                self.match_end_pos = m_rslt.end(0)
                _matched = True
                break

        return _matched

    @property
    def start(self):
        return TextStreamCursor(index=self.match_idx, position=self.match_start_pos)

    @property
    def end(self):
        return TextStreamCursor(index=self.match_idx, position=self.match_end_pos)


class PhraseTokenizer:
    def __init__(self, name):
        self.label_frequency = LabelFrequency.get(name)
        self.tokens_of_phrase = []
        self.is_invalid_phrase = lambda e: self.re_num_patn.match(e[0]) or self.re_num_patn.match(e[1])

    def recognize_phrases(self):
        """
            Calculate the frequency of all bigrams, and select the most common 1% as phrases
        """
        phrase_partitions = self.label_frequency.get_partitions(invalid_key=self.is_invalid_phrase)
        phrase_threshold = self.label_frequency.sample_size//100
        most_common_phrases = phrase_partitions.of_freq_greater(and_equal_to=phrase_threshold)
        for phrase, _ in self.most_common_phrases:
            self.tokens_of_phrase.append(Category.get(phrase))

        return most_common_phrases

    def tokenize_phrases(self, stream):
        for phrase in self.tokens_of_phrase:

            start_idx, start_pos = -1, -1
            start_cursor = TextStreamCursor(-1, -1)
            while start_idx < len(stream):
                is_find, two_words = self.find_phrase_in_stream(phrase, stream, start_cursor)
                if not is_find:
                    break

                # Check contents between two words
                retry = self.check_phrase_n_retry(two_words, stream)
                the_1st_word, the_2nd_word = two_words
                if retry:
                    # Retry from the end position of the first word
                    start_idx, start_pos = the_1st_word.match_idx, the_1st_word.match_end_pos
                    continue

                stream = self.rebuild_stream(two_words, stream)
                break
        return stream

    @staticmethod
    def find_phrase_in_stream(phrase, stream, start_cursor):
        if not isinstance(phrase, Category):
            raise TypeError('Argument "phrase" of method "find_phrase_in_stream" should be type Category '
                            'instead of {0!s}'.format(type(phrase)))
        the_two_words = (SlicingInStream(phrase.name[0]), SlicingInStream(phrase.name[1]))

        # Find the FIRST word in a phrase
        if not the_two_words[0].find_word(stream=stream, start_cursor=start_cursor):
            return False, None
            # Find the SECOND word, only when the first is found
        if not the_two_words[1].find_word(stream=stream, start_cursor=the_two_words[0].end):
            return False, None
        return True, the_two_words

    @staticmethod
    def check_phrase_n_retry(the_two_words, stream):
        the_1st_word, the_2nd_word = the_two_words
        retry = False
        _bf, chk_lst, _af = stream.split_stream(start_with=the_1st_word.end, end_before=the_2nd_word.start)
        for t in chk_lst:
            curr_txt = str(t) if isinstance(t, DocxToken) else t
            # If non-punctuation characters occurs, retry from the end position of the first word
            if not RuleServiceVarName.re_del_punct.match(curr_txt) and not re.match(r'^\s*$', curr_txt):
                retry = True
                break
        return retry

    @staticmethod
    def rebuild_stream(the_two_words, stream):
        new_stream = []

        the_1st_word, the_2nd_word = the_two_words
        # Up to now, a valid phrase is found, and new Token Phrase(word_1st, word_2nd)
        bf_stream, phrase_strings, af_stream = stream.split_stream(start_with=the_1st_word.start,
                                                                   end_before=the_2nd_word.end)

        new_stream.extend(bf_stream)
        # Any characters between the two words in a phrase will be deleted
        phrase = Phrase.get(' '.join(phrase_strings))
        phrase.token = Category.get(name=(the_1st_word.word, the_2nd_word.word))
        new_stream.append(phrase)
        new_stream.extend(af_stream)
        return new_stream

