# encoding = utf-8
__author__ = 'mochenx'

from RegDocxParser.Tokens.Category import Category
from RegDocxParser.NLP.Label import Label
from RegDocxParser.NLP.LabelSimilarity import LabelSimilarityMatrix, SimilarLabelsGroups
import functools


class SimilariryClassifier:
    def __init__(self, preset_categories=None):
        self.preset_categories = preset_categories
        self._majority = None
        self._minority = None

    def load_labels(self, majority_labels=None, minority_labels=None):
        if majority_labels is not None:
            self.majority = majority_labels
        if minority_labels is not None:
            self.minority = minority_labels

    @property
    def majority(self):
        return self._majority

    @majority.setter
    def majority(self, majority_labels):
        for label in majority_labels:
            if not isinstance(label, Label):
                raise TypeError('The RHS values of majority setter should be subclass type of Label'
                                'instead of {0!s}'.format(type(label)))
        self._majority = majority_labels

    @property
    def minority(self):
        return self._minority

    @minority.setter
    def minority(self, minority_labels):
        for label in minority_labels:
            if not isinstance(label, Label):
                raise TypeError('The RHS values of minority setter should be subclass type of Label'
                                'instead of {0!s}'.format(type(label)))
        self._minority = minority_labels

    def classify(self):
        grp_majority_n_minority, grp_minorities = self.similarity_match()
        return self.refill_similar_categories(grp_majority_n_minority, grp_minorities)

    def similarity_match(self):
        """
            Find similar labels in minority labels, and append them to majority labels
        """
        grp_majority_n_minority = self.get_similar_groups(x_labels=self.minority,
                                                          y_labels=self.majority)
        grp_minorities = self.get_similar_groups(x_labels=self.minority,
                                                 y_labels=self.minority)
        grp_remains = self.get_remain_groups(self.minority, grp_majority_n_minority, grp_minorities)
        grp_majority_n_minority.draw_similarity_from(grp_minorities)
        grp_minorities.append_groups(grp_remains)
        return grp_majority_n_minority, grp_minorities

    def refill_similar_categories(self, grp_majority_n_minority, grp_minorities):
        """
            For labels, both majority and minority, whose token are known but not being linked with,
            fill their token property with correct token
        """
        grp_majority_n_minority.refill_similar_categories()
        if grp_minorities is None or self.preset_categories is None:
            return grp_majority_n_minority, grp_minorities
        for group in grp_minorities.groups:
            exited_token = None
            for similar_label in group:
                str_similar_label = str(similar_label)
                if str_similar_label in self.preset_categories:
                    exited_token = Category.get(self.preset_categories[str_similar_label])
            if exited_token is None:
                continue
            for similar_label in group:
                if similar_label.token is None:
                    similar_label.token = exited_token
        return grp_majority_n_minority, grp_minorities

    @staticmethod
    def get_similar_groups(x_labels, y_labels):
        """
            1. Build a similarity matrix from given x & y labels
            2. Build similarity groups from fetched coordinates whose similarity scores <= 0.5
        """
        similarity_matrix = LabelSimilarityMatrix(y=y_labels, x=x_labels)
        similarity_matrix.fill_edit_distances()
        coordinates = similarity_matrix.get_coordinates_of(less_and_equal_to=0.5, greater_than=0)
        groups = SimilarLabelsGroups()
        groups.build_groups_from(coordinates)
        return groups

    @staticmethod
    def get_remain_groups(unrecognized_labels, *groups):
        remain_names = []
        for label in unrecognized_labels:
            str_label = label if isinstance(label, str) else str(label)
            is_not_in_group = lambda acc, group: (acc if not acc else
                                                  False if str_label in group.group_labels else True)
            not_in_a_group = functools.reduce(is_not_in_group, groups, True)
            if not_in_a_group:
                remain_names.append(label)

        groups = SimilarLabelsGroups()
        groups.build_groups_from(remain_names)
        return groups
