# encoding = utf-8
__author__ = 'mochenx'

import os
import re

import nltk
from nltk.metrics import edit_distance as edit_distance
from nltk.corpus import wordnet
from RegDocxParser.NLP.Label import Label, MetaLabelCreator


class SimilarLabel(metaclass=MetaLabelCreator):
    """
        The vertex of graph formed by similar labels. The edges of each vertex is not kept, instead of that neighbors
        are saved in an internal list '_other_similar_labels'
    """
    def __init__(self, similar_labels=None):
        self._label = similar_labels
        self._other_similar_labels = []

    @classmethod
    def create_with_name(cls, label):
        return SimilarLabel(label)

    @property
    def neighbors(self):
        return self._other_similar_labels

    def append_neighbor(self, neighbor):
        if neighbor not in self.neighbors:
            self._other_similar_labels.append(neighbor)

    @property
    def token(self):
        return self.label.token if hasattr(self.label, 'token') else None

    @token.setter
    def token(self, token_or_its_name):
        if not hasattr(self.label, 'token'):
            raise TypeError("The 'label' attribute of current SimilarLabel object doesn't contain a token attribute")
        self.label.token = token_or_its_name

    @property
    def label(self):
        return self._label

    def __str__(self):
        return self.label if isinstance(self.label, str) else str(self.label)


class SimilarLabelsGroups:
    """
        With methods of building graphs from given similar label relationships, which are the edges of graphs,
        and find paths in these graphs to form each similarity group.
    """
    def __init__(self):
        self.all_label_names = set()
        self.groups = []

    @staticmethod
    def is_valid_label_types(*labels):
        for label in labels:
            if not isinstance(label, str) and not isinstance(label, Label):
                raise TypeError('Only string and Label(and its subclass) object is supported '
                                'by SimilarLabelsGroups, {0} is not support'.format(type(label)))
        return True

    def build_groups_from(self, similar_label_tuples):
        # TODO: Is current solution proper, because SimilarLabel may not clean when calling build_groups_from
        for labels in similar_label_tuples:
            if isinstance(labels, tuple) and self.is_valid_label_types(*labels):
                self._append_linked_labels(*labels)
            elif self.is_valid_label_types(labels):
                self._append_linked_labels(labels, labels)
        self._build_groups()

    def _append_linked_labels(self, label1_str, label2_str):
        label1 = self._append_new_label(label1_str)
        label2 = self._append_new_label(label2_str)
        label1.append_neighbor(label2)
        label2.append_neighbor(label1)

    def _append_new_label(self, label_str):
        new_label = SimilarLabel.get(label_str)
        if new_label not in self.all_label_names:
            self.all_label_names.add(new_label)
        return new_label

    def _build_groups(self):
        """
            Do graph traversal with Depth-First-Search(DFS).
            For each unconnected components in graph, build a group for it.
        """
        self.groups.clear()
        while len(self.all_label_names) > 0:
            the_1st_label = self.all_label_names.pop()
            new_group = self._graph_dfs(the_1st_label, set())
            for label in new_group:
                self.all_label_names.discard(label)
            self.groups.append(new_group)

    @staticmethod
    def _graph_dfs(vertex, explored_vertexes):
        """
            Recursive version of Depth-First-Search(DFS)
        """
        following_vertexes = [vertex]

        explored_vertexes.add(vertex)
        for neighbor in vertex.neighbors:
            if neighbor not in explored_vertexes:
                following_vertexes.extend(SimilarLabelsGroups._graph_dfs(neighbor, explored_vertexes))
        return following_vertexes

    def draw_similarity_from(self, another_groups):
        stringify_group = lambda g: [str(label) for label in g]
        for group in self.groups:
            for i, another_group in enumerate(another_groups.groups):
                overlape = set(stringify_group(group)) & set(stringify_group(another_group))
                if len(overlape) == 0:
                    continue

                for label in another_group:
                    if label not in group:
                        group.append(label)
                another_groups.groups.pop(i)
                break

    def refill_similar_categories(self):
        """
            For labels whose token are known but not being linked with, fill their token property with correct token
        """
        for group in self.groups:
            exited_token = None
            for similar_label in group:
                if similar_label.token is not None:
                    exited_token = similar_label.token
            if exited_token is None:
                continue
            for similar_label in group:
                if similar_label.token is None:
                    similar_label.token = exited_token

    def append_groups(self, another_groups):
        for group in another_groups.groups:
            self.groups.append(group)

    @property
    def labels(self):
        return set([str(l) for l in self.all_label_names])

    @property
    def group_labels(self):
        str_all_labels = [str(label) if not isinstance(label, str) else label
                          for group in self.groups
                          for label in group]
        return str_all_labels

    def __str__(self):
        str_of_groups = []
        for group in self.groups:
            str_of_groups.append(','.join([str(l) for l in group]))
        return '\n'.join(str_of_groups)


class LabelSimilarityMatrix(dict):
    """
       ColumnTitleMatrix takes the charge of computing edit distances between recognized titles and unrecognized ones
    """
    get_str_from = lambda e: e if isinstance(e, str) else str(e)

    def __init__(self, x, y):
        for title_in_y in y:
            row = {}
            for title_in_x in x:
                row[title_in_x] = None
            self[title_in_y] = row
        nltk.data.path = [os.path.join(os.path.curdir, 'nltk_data')]

    @staticmethod
    def is_single_word(phrase):
        if re.search(r'\s+', phrase):
            return False
        return True

    @staticmethod
    def is_in_wordnet(word):
        sets = wordnet.synsets(word)
        if len(sets) == 0:
            return False
        return True

    @staticmethod
    def dont_calc_edit_distance(*args):
        dont_calc = True
        for word in args:
            if not LabelSimilarityMatrix.is_single_word(word) or not LabelSimilarityMatrix.is_in_wordnet(word):
                # Current word is a phrase or it's not an known English word
                dont_calc = False
        return dont_calc

    def fill_edit_distances(self):

        for y, row in self.items():
            for x in row.keys():
                s1 = LabelSimilarityMatrix.get_str_from(x)
                s2 = LabelSimilarityMatrix.get_str_from(y)
                if self.dont_calc_edit_distance(s1, s2):
                    row[x] = float('inf')
                else:
                    row[x] = edit_distance(s1, s2)/min(len(s1), len(s2))

    @staticmethod
    def del_duplicated_coordinates(coordinates):
        existed_pairs = {}
        for coordinate in coordinates:
            coord_set = frozenset(coordinate)
            if coord_set not in existed_pairs:
                existed_pairs[coord_set] = True
        return [tuple(c) for c in existed_pairs.keys()]

    def get_coordinates_of(self, less_and_equal_to=None, less_than=None,
                           greater_and_equal_to=None, greater_than=None):
        coordinates = []
        for y, row in self.items():
            for x, v in row.items():
                valid_v = True
                if less_and_equal_to is not None and v > less_and_equal_to:
                    valid_v = False
                elif less_than is not None and v >= less_than:
                    valid_v = False

                if greater_and_equal_to is not None and v < greater_and_equal_to:
                    valid_v = False
                elif greater_than is not None and v <= greater_than:
                    valid_v = False

                if valid_v:
                    coordinates.append((x, y))
        return self.del_duplicated_coordinates(coordinates)

    def __str__(self):
        def add_leading_spaces(cell_size, cell_content):
            leading_spaces = ' '*(cell_size - len(cell_content))
            return '{0}{1}'.format(leading_spaces, cell_content)

        def build_title_row(title_row, max_len_of_y_title):
            x_title_ident = [' '*(max_len_of_y_title + 1)]  # One more space after y_title for each row
            x_titles = [LabelSimilarityMatrix.get_str_from(x) for x in sorted(title_row.keys())]
            x_title_ident.extend(x_titles)
            return '|'.join(x_title_ident)

        def prepare_for_y_titles():
            y_titles = [LabelSimilarityMatrix.get_str_from(y) for y in sorted(self.keys())]
            return len(max(y_titles, key=lambda e: len(e)))

        s_print = []
        x_titles = None
        max_len_of_y_title = prepare_for_y_titles()

        for y_title, row in sorted(self.items(), key=lambda e: e[0]):
            if x_titles is None:
                x_titles = build_title_row(row, max_len_of_y_title)
                s_print.append(x_titles)

            s_print_row = [add_leading_spaces(max_len_of_y_title, y_title)]
            for x_title, v in sorted(row.items(), key=lambda e: e[0]):
                s_print_row.append(add_leading_spaces(len(x_title), '{0:.2f}'.format(v)))
            s_print.append(' '.join(s_print_row))
        return '\n'.join(s_print)
