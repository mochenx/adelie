# encoding = utf-8
__author__ = 'mochenx'

from RegDocxParser.NLP.Label import MetaLabelCreator
from RegDocxParser.Tokens.Category import Category


class ColumnTitle(metaclass=MetaLabelCreator):
    """
        A subclass of Label, customized by MetaLabelCreator, for preserving unique column title in a table
    """

    def __init__(self, column_title):
        self.title = column_title
        self._token = None

    def __str__(self):
        return self.title

    @classmethod
    def create_with_name(cls, column_title):
        return ColumnTitle(column_title)

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, token_or_its_name):
        if isinstance(token_or_its_name, str):
            self._token = Category.get(name=token_or_its_name)
        elif isinstance(token_or_its_name, Category):
            self._token = token_or_its_name

    @property
    def name_number(self):
        return len(self._all_names)