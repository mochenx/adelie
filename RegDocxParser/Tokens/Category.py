# encoding = utf-8
__author__ = 'mochenx'

import sqlite3
from RegDocxParser.NLP.Label import MetaLabelCreator


class Category(metaclass=MetaLabelCreator):
    """
        A subclass of Label, customized by MetaLabelCreator, for preserving the token type of a column title
        For example:
            'Default', 'Def Value', 'Default Value', 'De Value' are different column titles, but they could be the same
            token type
    """

    def __init__(self, token_name):
        self._name = token_name

    @classmethod
    def create_with_name(cls, name):
        return Category(name)

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, token_name):
        self._name = token_name

    def save_to_sqlite(self, file_name):
        conn = sqlite3.connect(file_name)
        db_c = conn.cursor()
        # conn.commit()
        conn.close()

    def load_from_sqlite(self, file_name):
        pass

    def __str__(self):
        return self.name if isinstance(self.name, str) else str(self.name)

# The following codes are used as reference for supporting SQLite
# def create_table(db_c, table_name, fields):
#     """
#         Create Table in DB, when the same named table exists, overwrite it or append to it.
#     """
#     db_c.execute(''' SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;''')
#     for row in db_c:
#         if dbg_mode:
#             print(row)
#         if append_to_exist_table and row[0] == table_name:
#             # Append
#             print('Table {0} exists, and current script will append data into it'.format(table_name))
#             return
#         elif row[0] == table_name:
#             # Overwrite
#             print('Table {0} exists, current script will recreate the table, existed data will lose'.format(table_name))
#             db_c.execute(r'DROP TABLE {0}'.format(table_name))
#     db_c.execute(r'CREATE TABLE {0} ({1})'.format(table_name, ', '.join(fields)))

# def insert2table(db_c, table_name, vals):
#     try:
#         s_insert_vals = ','.join(vals)
#         db_c.execute(u"INSERT INTO {0} VALUES ({1})".format(table_name, s_insert_vals))
#     except sqlite3.OperationalError:
#         print(','.join(vals))

# def log_sqlite_db(limit=None):
#     """
#         Log all or indicated number of SQLiteDB data into a text file.
#     """
#     conn = sqlite3.connect(sqlite_db_name)
#     db_c = conn.cursor()
#     db_c.execute(' SELECT * FROM {0} '.format(sqlite_table_name))
#
#     cnt = 0
#     with codecs.open('sqlite_db_data.dbg.log', 'w', encoding='utf8') as f:
#         for row in db_c:
#             f.write(u'{0}{1}'.format(u', '.join(row), os.linesep))
#             if limit and isinstance(limit, int) and limit == cnt:
#                 break
#             cnt += 1
