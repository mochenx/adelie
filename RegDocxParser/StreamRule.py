# encoding = utf-8
__author__ = 'mochenx'

import re
from RegDocxParser.compat import py3k
import logging


class RuleRelation(object):
    def __init__(self, priority, left=None, right=None):
        self.priority = priority
        self.left_operand = None
        self.right_operand = None
        self._left = None
        self._right = None
        if left and isinstance(left, list):
            self.left_operand = left
        if right and isinstance(right, list):
            self.right_operand = right

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, val):
        self._left = val

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, val):
        self._right = val

    @property
    def path(self):
        if self.left:
            self.left_operand = self.left.path if isinstance(self.left, RuleRelation) else [[self.left]]
        if self.right:
            self.right_operand = self.right.path if isinstance(self.right, RuleRelation) else [[self.right]]
        return self.operate()

    def __lt__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority < other.priority else False

    def __le__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority <= other.priority else False

    def __gt__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority > other.priority else False

    def __ge__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority >= other.priority else False

    def __eq__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority == other.priority else False

    def __ne__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if self.priority != other.priority else False

    def operate(self):
        raise NotImplementedError('Subclass of RuleRelation must implement this method')


class RuleRelationOr(RuleRelation):
    """
        A . B | C means A is concatenated with B or C
    """
    def __init__(self, left=None, right=None):
        super(RuleRelationOr, self).__init__(11, left, right)

    def __str__(self):
        return '|'

    def operate(self):
        rslt = []
        for lst in self.left_operand:
            rslt.append(lst)

        for lst in self.right_operand:
            rslt.append(lst)
        return rslt


class RuleRelationDot(RuleRelation):
    """
        A . B | C means A is concatenated with B or C
    """
    def __init__(self, left=None, right=None):
        super(RuleRelationDot, self).__init__(10, left, right)

    def __str__(self):
        return '.'

    def operate(self):
        rslt = []
        for l_lst in self.left_operand:
            for r_lst in self.right_operand:
                cpy_l_lst = l_lst[:]
                cpy_r_lst = r_lst[:]
                cpy_l_lst.extend(cpy_r_lst)
                rslt.append(cpy_l_lst)
        return rslt


class RuleRelationLParenthes(RuleRelation):
    def __init__(self):
        super(RuleRelationLParenthes, self).__init__(100)

    def __str__(self):
        return '('

    def __lt__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return False

    def __le__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return False

    def __gt__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return False

    def __ge__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return False

    def __eq__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return True if isinstance(other, RuleRelationLParenthes) else False

    def __ne__(self, other):
        if not other or not isinstance(other, RuleRelation):
            raise TypeError
        return False if isinstance(other, RuleRelationLParenthes) else True


class RuleRelationRParenthes(RuleRelation):
    def __init__(self):
        super(RuleRelationRParenthes, self).__init__(0)

    def __str__(self):
        return ')'


class RuleNodeWrapper(RuleRelation):
    def __init__(self, wrapperd_val):
        super(RuleNodeWrapper, self).__init__(99)
        self._val = [wrapperd_val] if isinstance(wrapperd_val, list) else [[wrapperd_val]]

    def __str__(self):
        return '__Root__'

    @property
    def path(self):
        return self._val


class StreamRule(object):
    """
        Base class of Rule hierarchies. All rules are fitted into three steps: Pre, Stream and Post.
        For each step, activated rules are grouped by list or set. Users can utilize class method of StreamRule
        to manipulate and apply these rules
    """
    # TODO: Implement '|' & '.(+)' operation on StreamRule
    # Pre, Stream and Post rules are grouped by list or set, list means ordered rules and set means selection.
    # As a result, all rules are linked to form several paths like:
    # Pre: ['Rule1', set('Rule2_1', 'Rule2_2'), 'Rule3']
    # Stream: set(['Rule4_1', 'Rule5'], ['Rule4_2'])
    # Post: ['Rule6']
    # Then, paths
    # Rule1 -> Rule2_1 -> Rule3 -> { Rule4_1 -> Rule5 }* -> Rule6
    # Rule1 -> Rule2_2 -> Rule3 -> { Rule4_1 -> Rule5 }* -> Rule6
    # Rule1 -> Rule2_1 -> Rule3 -> { Rule4_2 }* -> Rule6
    # Rule1 -> Rule2_2 -> Rule3 -> { Rule4_2 }* -> Rule6
    pre_rules = None
    stream_rules = None # [set(RuleSplitPhrase(), RuleSplitPhrase(3)), ]
    post_rules = None

    f_print_rule_rslt = lambda e: ','.join([(c if isinstance(c, str) else str(c)) for c in e])

    @classmethod
    def build_infix_rule(cls, input_rule):
        """
            Build infix list recursively, input rule is described as
                [RuleInUT('RuleInUT_0'),
                set([RuleInUT('RuleInUT_1'), RuleInUT('RuleInUT_2')]),
                RuleInUT('RuleInUT_3')]
        """
        if isinstance(input_rule, StreamRule):
            infix_rule = input_rule
        elif isinstance(input_rule, list) or isinstance(input_rule, tuple):
            # For the reason that the member in a set must be immutable, nested list MUST be object of tuple
            infix_rule = [RuleRelationLParenthes()]
            for rule in input_rule:
                # Recursively calling
                rule_in_sublayer = StreamRule.build_infix_rule(rule)
                # Rule
                if isinstance(rule_in_sublayer, list):
                    infix_rule.extend(rule_in_sublayer)
                else:
                    infix_rule.append(rule_in_sublayer)
                # Dot
                infix_rule.append(RuleRelationDot())
            # Replace the latest '.' operator with ')'
            infix_rule[-1] = RuleRelationRParenthes()
        elif isinstance(input_rule, set) or isinstance(input_rule, frozenset):
            # For the reason that the member in a set must be immutable, nested set MUST be object of frozenset
            infix_rule = [RuleRelationLParenthes()]
            for rule in input_rule:
                # Recursively calling
                rule_in_sublayer = StreamRule.build_infix_rule(rule)
                # Rule
                if isinstance(rule_in_sublayer, list):
                    infix_rule.extend(rule_in_sublayer)
                else:
                    infix_rule.append(rule_in_sublayer)
                # Or
                infix_rule.append(RuleRelationOr())
            # Replace the latest '|' operator with ')'
            infix_rule[-1] = RuleRelationRParenthes()
        else:
            raise TypeError('Input rules contain a unsupported type')
        return infix_rule

    @classmethod
    def infix_to_postfix(cls, input_rule):
        stack_obj = []
        stack_op = []
        if not isinstance(input_rule, list):
            raise TypeError('Input rules contain a unsupported type')
        for r in input_rule:
            if isinstance(r, RuleRelation):
                # An operator element
                while True:
                    if isinstance(r, RuleRelationLParenthes) or len(stack_op) == 0:
                        stack_op.append(r)
                        break
                    elif isinstance(r, RuleRelationRParenthes) and isinstance(stack_op[-1], RuleRelationLParenthes):
                        # To current branch, only when len(stack_op) > 0 and r ISN'T a RuleRelationLParenthes object
                        # Drop both left & right parentheses
                        stack_op.pop()
                        break
                    elif len(stack_op) > 0 and stack_op[-1] >= r:
                        # clear all between current parentheses and the nearest left one when r is right parentheses
                        # The priority of right parentheses is ZERO, the lowest
                        stack_obj.append(stack_op.pop())
                    else:
                        # len(stack_op) > 0 and stack_op[-1] < r:
                        stack_op.append(r)
                        break
            else:
                stack_obj.append(r)
        return stack_obj

    @classmethod
    def postfix_to_parsingtree(cls, input_rule):
        if not isinstance(input_rule, list):
            raise TypeError('Input rules contain a unsupported type')

        parsing_stack = []
        pt_root = None
        for i, r in enumerate(input_rule):
            if isinstance(r, RuleRelation):
                # An operator element
                # Judge the index or NOT?
                r.left, r.right = parsing_stack[-2], parsing_stack[-1]
                parsing_stack[-2] = r
                parsing_stack.pop()
            else:
                parsing_stack.append(r)
            pt_root = r
        # Check the stack
        if len(parsing_stack) != 1:
            raise ValueError('Stack should only contain the root of parsing tree after parsing')

        return RuleNodeWrapper(pt_root) if isinstance(pt_root, StreamRule) else pt_root

    @classmethod
    def _get_path(cls, rules):
        # Build parsing tree from Set/List Expression
        parsed_infix_rule = StreamRule.build_infix_rule(rules)
        postfix = StreamRule.infix_to_postfix(parsed_infix_rule)
        pt = StreamRule.postfix_to_parsingtree(postfix)

        # Get ALL available paths
        return pt.path

    @classmethod
    def links(cls, rules):
        """
            Return an iterator to iterate all possible links
            Return a list
        """
        if not rules:
            yield False

        if not rules:
            return
        all_path = StreamRule._get_path(rules)
        for _p in all_path:
            yield _p

    @classmethod
    def rules(cls, curr_link):
        """
            Return an iterator to iterate all rules in current link
            Return a subclass of StreamRule
        """
        for r in curr_link:
            yield r

    def __init__(self):
        self._rule_name = 'StreamRule'

    def __str__(self):
        return self._rule_name

    def apply(self, stream_obj):
        raise NotImplementedError('Method apply need to be overrided in sub class')

    def set_logger(self, name):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)
        # create file handler which logs even debug messages
        fh = logging.FileHandler(name + '.log', mode='w', encoding='utf-8')
        fh.setLevel(logging.DEBUG)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('::%(name)s - %(source)s - %(message)s')
        ch.setFormatter(formatter)
        fh.setFormatter(formatter)
        # add the handlers to logger
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

