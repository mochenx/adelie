# encoding = utf-8
__author__ = 'mochenx'

from docx import Document


from docx.oxml.table import CT_Tbl
from docx.oxml.text import CT_P

from docx.table import _Cell, Table
from docx.text import Paragraph

import os

from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.BuiltinRules.RuleNumAssign import RuleNumAssign
from RegDocxParser.BuiltinRules.RuleLineSplitter import RuleLineSplitter
from RegDocxParser.Stream import DocxStream, TextStream
from RegDocxParser.Stream.TableStream import TableStream

class DocxParser(object):

    def __init__(self, docx_file_name):
        self.parsed_docx = Document(docx_file_name)
        self._vdocx = Document()

    def __iter__(self):
        """
        Yield each paragraph and table child within *parent*, in document order.
        Each returned value is an instance of either Table or Paragraph.
        """
        if isinstance(self.parsed_docx, Document):
            parent_elm = self.parsed_docx._document_part.body._body
        elif isinstance(self.parsed_docx, _Cell):
            parent_elm = self.parsed_docx._tc
        else:
            raise ValueError("something's not right")

        for child in parent_elm.iterchildren():
            if isinstance(child, CT_P):
                yield Paragraph(child, self._vdocx._document_part._element.body)
            elif isinstance(child, CT_Tbl):
                yield Table(child, self._vdocx._document_part._element.body)

    def parse(self):
        """
            1. Load docx, and create *Stream objects for each Paragraph or Table
            2. Build a DocxStream
            3. Apply all pre-rules to DocxStream
            4. For each link in the rules, apply each rule in current link to every *Stream in a DocxStream
            5. Apply all post-rules to DocxStream
            6. Tokenize DocxStream
            7. Score DocxStream and store the max scored DocxStream, go back to step 3 until running out of all links
        """
        # Step 1. Create *Stream objects for each Paragraph or Table
        # Step 2. Build a DocxStream
        self.docx_stream = DocxStream()
        for o in self:
            if isinstance(o, Paragraph) and o.text:
                self.docx_stream.append(TextStream(raw_data=o))
            elif isinstance(o, Table):
                self.docx_stream.append(TableStream(raw_data=o))

        score_max_stream = None
        for pre_lnk in StreamRule.links(StreamRule.pre_rules):
            # Step 3. Apply all pre-rules to DocxStream
            after_pre_rule = self.docx_stream.clone(only_raw=True)
            if pre_lnk:
                for r in StreamRule.rules(curr_link=pre_lnk):
                    r.apply(after_pre_rule)

            # Step 4. For each link in the rules, apply each rule in current link to every *Stream in a DocxStream
            for stream_lnk in StreamRule.links(StreamRule.stream_rules):
                eval_stream = after_pre_rule.clone(only_raw=False)
                if stream_lnk:
                    for a_stream in eval_stream:
                        for r in StreamRule.rules(curr_link=stream_lnk):
                            r.apply(a_stream)

                # Step 5. Apply all post-rules to DocxStream
                for post_lnk in StreamRule.links(StreamRule.post_rules):
                    for_post_rule = eval_stream.clone(only_raw=False)
                    if post_lnk:
                        for r in StreamRule.rules(curr_link=post_lnk):
                            r.apply(for_post_rule)

                    # Step 6. Tokenize DocxStream

                    # Step 7. Score DocxStream and store the max scored DocxStream,
                    #   and go back to step 3 until running out of all links
                    score_max_stream = (for_post_rule if not score_max_stream else
                                        for_post_rule if for_post_rule.score > score_max_stream.score
                                        else score_max_stream)

        self.docx_stream = score_max_stream
        return score_max_stream


if __name__ == '__main__':
    """
        tc_docx/MassTest.docx
    """
    r1 = RuleLineSplitter()
    r2 = RuleNumAssign()
    StreamRule.stream_rules = [r1, r2]
    # reg_docx = DocxParser(docx_file_name=os.path.join('tc_docx', 'short.docx'))
    reg_docx = DocxParser(docx_file_name=os.path.join('tc_docx', 'MassTest.docx'))
    docx_stream = reg_docx.parse()
    cnt = 0
    # whole_para =
    ana_log = []
    with open('ana.log', 'w', encoding='utf8') as f:
        for o in docx_stream:
            if not o.stream:
                continue
            curr_ln = o.stream
            # ana_log.extend([c for c in curr_ln])
            # s = {'Token': 'String', 'Text': [c for c in curr_ln]}
            # f.write('{0}{1}'.format(json.dumps(s), os.linesep))
            # f.write('\r'.join([c for c in curr_ln]))
            s = '{0}'.format(os.linesep).join([(c if isinstance(c, str) else str(c)) for c in curr_ln]) + os.linesep
            f.write(s)
            cnt += 1
            # if cnt >= 1000:
            #     exit()
            # if isinstance(o, Paragraph) and o.text:
            #     curr_ln = r1.apply(o.text)
            #     # curr_ln = re.sub(SENTENCE_END_SYMBOL, '\n', o.text).splitlines()
            #     s = {'Token': 'String', 'Text': [c for c in curr_ln]}
            #     f.write('{0}{1}'.format(json.dumps(s), os.linesep))
            #     cnt += 1
            #     if cnt >= 1000:
            #         exit()
