# encoding = utf-8
__author__ = 'mochenx'

import re
from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.Stream import TextStream


class TokenSoftBreak(DocxToken):
    def __init__(self, break_op):
        self.break_op = break_op

    def __str__(self):
        return self.break_op

    def __repr__(self):
        return '<<TokenSoftBreak[[{0}]]>>'.format(self.break_op)


class TokenHardBreak(DocxToken):
    def __str__(self):
        return '<CR>'

    def __repr__(self):
        return '<<TokenHardBreak>>'


class RuleLineSplitter(StreamRule):
    line_break_patn = r'(\r|\n|(\r\n))'

    def __init__(self, split_space_num=2):
        """
            'split_space_num' or more spaces are considered as a line break
        """
        self.soft_break_patn = r'(({0}+)|\t)'.format(' '*split_space_num)
        # self.RE_PHRASE_END_SYMBOL = re.compile(r'({0}+)|\t|\.|;'.format(' '*split_space_num))
        super().set_logger(__name__)

    @staticmethod
    def build_hard_break(text, start_idx, m_rslt):
        before_str = text[start_idx: m_rslt.start(1)] if start_idx == m_rslt.start(1) else None
        return before_str, TokenHardBreak.get(), m_rslt.end(1)

    @staticmethod
    def build_soft_break(text, start_idx, m_rslt):
        before_str = text[start_idx: m_rslt.start(1)] if start_idx < m_rslt.start(1) else None
        return before_str, TokenSoftBreak(m_rslt.group(1)), m_rslt.end(1)

    def apply(self, stream_obj):
        """
            Attention Please, RuleLineSpliter should be the FIRST stream rule in rule links, because it produces
            stream property of input stream object.
        """
        if not isinstance(stream_obj, TextStream):
            return None

        # Split input string into sentences by hard break
        rslt_stream = DocxToken.sub_re_with_token(stream_obj.raw.text, RuleLineSplitter.line_break_patn,
                                                  RuleLineSplitter.build_hard_break)
        # Eliminate only-space-sentence
        rslt_stream = [s for s in rslt_stream if isinstance(s, DocxToken) or not re.match('^$', s)]
        sentences = rslt_stream[:]

        # Split sentences into phases by soft break
        rslt_stream = []
        for sentence in sentences:
            if isinstance(sentence, DocxToken):
                rslt_stream.append(sentence)
                continue
            rslt_stream.extend(DocxToken.sub_re_with_token(sentence, self.soft_break_patn,
                                                           RuleLineSplitter.build_soft_break))
        del sentences
        # Now, rslt_stream is a list of phrases
        # Eliminate only-space-phrase
        rslt_stream = [s for s in rslt_stream if isinstance(s, DocxToken) or not re.match('^$', s)]

        # Strip prefix & postfix spaces
        len_rslt = len(rslt_stream)
        for i in range(len_rslt):
            if isinstance(rslt_stream[-1-i], str):
                rslt_stream[-1-i] = rslt_stream[-1-i].lstrip(' ')
                rslt_stream[-1-i] = rslt_stream[-1-i].rstrip(' ')

        stream_obj.stream = rslt_stream
        self.logger.debug(stream_obj.raw.text, extra={'source': 'before'})
        self.logger.debug(StreamRule.f_print_rule_rslt(rslt_stream), extra={'source': 'after '})
        return rslt_stream

