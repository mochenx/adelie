# encoding = utf-8
__author__ = 'mochenx'

import re

from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.BuiltinRules.RuleService import RuleServiceNumber, RuleServiceVarName
from RegDocxParser.Tokens.Category import Category
from RegDocxParser.NLP.Label import LabelPartition
from RegDocxParser.NLP.ColumnTitle import ColumnTitle
from RegDocxParser.NLP.LabelClassifier import SimilariryClassifier
from RegDocxParser.Stream.TableStream import TableStream

import nltk
import nltk.data


class RuleTypTabTitleBase(StreamRule):
    all_titles = []
    all_title_words = []

    def put_titles(self, char_lst):
        self.all_titles.extend(char_lst)

    def put_words(self, char_lst):
        self.all_title_words.append(char_lst)


class RuleTypTabTitleCollector(RuleTypTabTitleBase):
    """
    """
    def __init__(self):
        """
        """
        super().set_logger(__name__)

    def apply(self, docx_stream):
        for a_stream in docx_stream:
            self.stream_apply(a_stream)

    @staticmethod
    def standardize_cell_text(cell_text):
        cell_text = cell_text.lstrip(' ').rstrip(' ')
        # Get rid of spaces
        cell_contents = [s.capitalize() for s in RuleServiceVarName.re_break_sent_punct.split(cell_text)
                         if not re.match(r'^\s*$', s)]
        # Capitalize all words in a given phrase
        cell_text = ' '.join([s.capitalize() for s in cell_contents if not re.match(r'^\s*$', s)])
        return cell_text

    def stream_apply(self, stream_obj):
        if not isinstance(stream_obj, TableStream):
            return None

        for row_idx, row in enumerate(stream_obj.stream):
            if row_idx > 0:
                break
            titles = []
            for col_idx, cell_txt in row.items():
                cell_txt = self.standardize_cell_text(cell_txt)
                titles.append(cell_txt)
                row[col_idx] = ColumnTitle.get(cell_txt)
                self.put_words(row[col_idx])
            self.put_titles(set(titles))


class RuleTypTabTitleBuilder(RuleTypTabTitleBase):
    """
    """
    re_num_patn = re.compile(RuleServiceNumber.num_patn)
    preset_categories = {}

    def __init__(self):
        """
        """

        self.first_run_apply = True
        self.classifier = SimilariryClassifier()
        super().set_logger(__name__)

    def apply(self, stream_obj):
        if self.first_run_apply:
            self.classify_titles()
            self.first_run_apply = False

    def classify_titles(self):
        recognized_phrases, unrecognized_phrases = self.recognize_titles()
        recognized_names = self.build_categories_from(recognized_phrases)
        unrecognized_names = [ColumnTitle.get(phrase) for phrase, _ in unrecognized_phrases]

        self.classifier.load_labels(recognized_names, unrecognized_names)
        grp_majority_n_minority, grp_minorities = self.classifier.classify()

        self.log_all_phrases(recognized_phrases, unrecognized_phrases)
        self.log_all_groups(grp_majority_n_minority, grp_minorities)

    def recognize_titles(self):
        """
            Calculate the frequency of all titles, and select the most common as phrases
        """
        stringified_title_words = [word if isinstance(word, str) else str(word) for word in self.all_title_words]
        self.logger.debug(self.all_title_words[0].name_number, extra={'source': 'ColumnTitle Object number'})
        phrases_with_freq =nltk.FreqDist(stringified_title_words)

        valid_phrases_with_freq = self.filter_invalid_phrases(phrases_with_freq)
        phrase_partitions = LabelPartition.build_from_freq(valid_phrases_with_freq)

        phrase_threshold = phrases_with_freq.N()//10
        recognized_phrases = phrase_partitions.of_freq_greater(and_equal_to=phrase_threshold)
        unrecognized_phrases = phrase_partitions.of_freq_less(than=phrase_threshold)

        return recognized_phrases, unrecognized_phrases

    @staticmethod
    def build_categories_from(phrases_with_freq):
        for phrase, _ in phrases_with_freq:
            title = ColumnTitle.get(phrase)
            title.token = phrase
        return Category.get_all_names()

    def filter_invalid_phrases(self, phrases_with_freq):
        """
            Filter number components in given phrases, and the frequency is untouched
        """
        is_invalid_phrase = lambda e: self.re_num_patn.match(e) or len(e) > 1000

        valid_phrases = []
        for phrase, freq in phrases_with_freq.items():
            if is_invalid_phrase(phrase):
                continue
            valid_phrases.append((phrase, freq))
        return valid_phrases

    def log_all_phrases(self, recognized_phrases, unrecognized_phrases):
        for p in recognized_phrases:
            self.logger.debug(p, extra={'source': 'Recognized'})
        for p in unrecognized_phrases:
            self.logger.debug(p, extra={'source': 'Unrecognized'})

    def log_all_groups(self, grp_majority_n_minority, grp_minorities):
        for group in grp_majority_n_minority.groups:
            for label in group:
                self.logger.debug('Label:{0!s} with Token:{1!s}'.format(label, label.token),
                                  extra={'source': 'Major Groups'})
        for group in grp_minorities.groups:
            for label in group:
                self.logger.debug('Label:{0!s} with Token:{1!s}'.format(label, label.token),
                                  extra={'source': 'Minor Groups'})
