# encoding = utf-8
__author__ = 'mochenx'


import re
from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.BuiltinRules.RuleService import RuleServiceNumber, RuleServiceVarName
from RegDocxParser.Stream import TextStream


class TokenHDLRange(DocxToken):
    all_hdl_ranges = {}

    def __init__(self, ext_name, var_name=None, dimension_ranges=None):
        self.ext_name = ext_name
        self.var_name = var_name
        self.dimension_ranges = dimension_ranges
        # new_tkn.probability['msb'] = RuleServiceNumber.get_num_probability(msb)
        self.probability = []
        TokenHDLRange.all_hdl_ranges[self.ext_name] = self

    def __str__(self):
        return self.ext_name

    def __repr__(self):
        return '<<TokenHDLRange[[{0}:{msb} {1}:{lsb}]]>>'.format(self.msb, self.lsb, **self.probability)

    # def get(cls, *args, force_new=False, **kwargs):
    #     pass


class RuleHDLRange(StreamRule):
    """
        [number]: Not HDL Range
        [number: number]: HDL Range
        var_name[number: number]: HDL Range
        var_name[number]: HDL Range
        var_name([number]|[number: number])+: HDL Range
    """
    bits_range = r'\[({0}) *: *({0})?\]'.format(RuleServiceNumber.dec_nums)
    bits_sel_or_range = r'\[({0}) *(: *({0}))?\]'.format(RuleServiceNumber.dec_nums)
    re_bits_sel_or_range = re.compile(bits_sel_or_range)
    re_var_with_range = re.compile(r' *({0})|({2}) *(({1})+) *'.format(bits_range, bits_sel_or_range,
                                                                       RuleServiceVarName.var_name))
    re_var_name = re.compile(r'^.*\b({0})\b *$'.format(RuleServiceVarName.var_name))
    f_underscore_quote = lambda e: '_' + e
    f_prepend_to = lambda e: 'to' + e

    def __init__(self):
        super().set_logger(__name__)

    def combine_var_name(self, stream_lst):
        """
            A function of scanning transformed stream, and combine nearby TokenHDLRange objects or
            string and TokenHDLRange object
            stream_lst: A list of string and tokens like
                ['xxx', 'xxx', TokenHDLRange, TokenHDLRange, OtherDocxTokens ...]
        """
        comb_stream_lst = []
        skip_nxt = False
        for i, t in enumerate(stream_lst):
            if skip_nxt:   # Current token has been combined with its precedence
                skip_nxt = False
                continue
            elif isinstance(t, DocxToken) and not isinstance(t, TokenHDLRange):
                # For other DocxToken objects, append it directly
                comb_stream_lst.append(t)
                continue

            remain_idx = 0
            # If current object is TokenHDLRange, convert it to string before RE matching
            curr_tkn = str(t) if isinstance(t, TokenHDLRange) else t
            m_rslt = self.re_var_name.match(curr_tkn)
            if (m_rslt and i < (len(stream_lst) - 1) and
                isinstance(stream_lst[i + 1], TokenHDLRange) and stream_lst[i + 1].var_name is None):
                # Append string before matching point
                bf_str = curr_tkn[remain_idx: m_rslt.start(1)]
                comb_stream_lst.append(bf_str)
                # Get next TokenHDLRange object
                nxt_tkn = stream_lst[i + 1]
                # Modify and append it, and current token/string is dropped
                nxt_tkn.var_name = m_rslt.group(1)
                nxt_tkn.ext_name = nxt_tkn.var_name + nxt_tkn.ext_name
                comb_stream_lst.append(nxt_tkn)
                skip_nxt = True
            else:
                comb_stream_lst.append(t)

        return comb_stream_lst

    def apply(self, stream_obj):
        if not isinstance(stream_obj, TextStream):
            return None
        if not isinstance(stream_obj.stream, list):
            raise TypeError('RuleHDLRange need a stream with list type')

        self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': ' '*5})
        splited_t_lst = []
        _matched = False
        for i, t in enumerate(stream_obj.stream):
            if isinstance(t, DocxToken):
                splited_t_lst.append(t)
                continue

            remain_idx = 0
            # Final RE of re_var_with_range:
            #   ' *(\[([1-9]\d*|0) *: *([1-9]\d*|0)?\])|([_\d\w]+) *((\[([1-9]\d*|0) *(: *([1-9]\d*|0))?\])+) *'
            #                                                                             |group8    |
            #         |group2    |     |group3    |                  |group6       |  |group7         |
            #      |group1                            | |group4  |  |group5                                 |
            for m_rslt in self.re_var_with_range.finditer(t):
                # For each phrase, find an assignment pattern

                range_only = m_rslt.group(1)
                if range_only:    # Only HDL Range [number: number]
                    var_name, bits_range = None, range_only
                    bf_end_idx = m_rslt.start(1)
                    nxt_remain_idx = m_rslt.end(1)
                else:
                    var_name, bits_range = m_rslt.group(4), m_rslt.group(5)
                    bf_end_idx = m_rslt.start(4)
                    nxt_remain_idx = m_rslt.end(5)
                s_range = []
                dimension_ranges = []
                # Final RE of re_bits_sel_or_range:
                #   '\[([1-9]\d*|0) *(: *([1-9]\d*|0))?\]'
                #                        |group3    |
                #      |group1    |  |group2         |
                for m_rslt_range in self.re_bits_sel_or_range.finditer(bits_range):
                    msb, lsb = m_rslt_range.group(1), m_rslt_range.group(3)
                    # msb = msb.lstrip(' ')  # msb will NOT contain spaces
                    s_range.append(RuleHDLRange.f_underscore_quote(msb) +
                                   (RuleHDLRange.f_prepend_to(lsb) if lsb else ''))
                    # lsb = lsb.lstrip(' ') if lsb else msb
                    lsb = lsb if lsb else msb
                    dimension_ranges.append((msb, lsb))
                s_range = ''.join(s_range)
                ext_tkn_name, var_name = (var_name + s_range, var_name) if var_name else (s_range, None)
                # String before token
                bf_str = t[remain_idx:bf_end_idx].lstrip(' ')
                bf_str = bf_str.rstrip(' ')
                if bf_str and bf_str != '':
                    splited_t_lst.append(bf_str)
                # New Token
                new_tkn = TokenHDLRange(ext_name=ext_tkn_name, var_name=var_name, dimension_ranges=dimension_ranges)
                splited_t_lst.append(new_tkn)
                remain_idx = nxt_remain_idx  # m_rslt.end(3)

                _matched = True

            # The remains
            if remain_idx < len(t) and not re.match(r'^\s+$', t[remain_idx:]):
                splited_t_lst.append(t[remain_idx:])

        # Combine nearby TokenHDLRange objects, or string and TokenHDLRange object
        splited_t_lst = self.combine_var_name(splited_t_lst)
        stream_obj.stream = splited_t_lst
        if _matched:
            self.logger.debug(StreamRule.f_print_rule_rslt(splited_t_lst), extra={'source': 'after'})
        return splited_t_lst
