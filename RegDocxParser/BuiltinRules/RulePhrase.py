# encoding=utf-8
__author__ = 'mochenx'

from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.BuiltinRules.RuleService import RuleServiceNumber, RuleServiceVarName
from RegDocxParser.NLP.Phrase import PhraseTokenizer
from RegDocxParser.NLP.Label import LabelFrequency
from RegDocxParser.Stream import TextStream
import re
import nltk
import nltk.data
import nltk.tokenize
from nltk.util import bigrams
import os

"""
The Rule of finding and replacing phrases with tokens
"""


class RulePhraseCollector(StreamRule):
    """
        The Rule of finding phrases in given stream
        RulePhraseCollector, as its name, only collects possible phrases(bigrams, with the help of NLTK) and
            saves them to label_frequency
    """
    def __init__(self):
        super().set_logger(__name__)
        nltk.data.path = [os.path.join(os.path.curdir, 'nltk_data')]
        self.sent_detector = nltk.data.load('tokenizers/punkt/PY3/english.pickle')
        self.label_frequency = LabelFrequency.get('ToGetPhrases')

    def apply(self, stream_obj):
        """
            Override method, it will scan and concatenate all items of a stream, then split them into sentences.
            Then, form bigrams for future use.
        """
        if not isinstance(stream_obj, TextStream):
            return None
        if not isinstance(stream_obj.stream, list):
            raise TypeError('RulePhraseCollector need a stream with list type')

        # self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': ' '*5})
        whole_stens = []
        for i, t in enumerate(stream_obj.stream):
            curr_txt = str(t) if isinstance(t, DocxToken) else t
            curr_txt = curr_txt.lstrip(' ').rstrip(' ')
            # Spaces separated words
            curr_txt_lst = [t for t in re.split(r'\s+', curr_txt) if t != '']
            whole_stens.extend(curr_txt_lst)

        # Join all words
        orig_sentence = ' '.join(whole_stens)
        # Into sentences
        sentences = self.sent_detector.tokenize(orig_sentence)
        collected_bigrams = []
        for a_sentence in sentences:
            # Del punctuations like #$%&'*+-/<=>@\^`|~()[]{}"
            sentence = RuleServiceVarName.re_del_punct.sub(' ', a_sentence)
            # And bigrams will NOT cross different sentences
            sentence = [s.capitalize() for s in RuleServiceVarName.re_break_sent_punct.split(sentence)
                        if not re.match(r'^\s*$', s)]
            collected_bigrams.extend(bigrams(sentence))
        self.label_frequency.collect(collected_bigrams)
        # self.logger.debug(StreamRule.f_print_rule_rslt(collected_bigrams), extra={'source': 'after'})

        return stream_obj.stream


class RulePhraseBuilder(StreamRule):
    """
        The Rule of replacing phrases with tokens
        RulePhraseBuilder use the distributions of collected bigrams to find phrases in method recognize_phrases with
            help of NLTK, and choose the most common 1% as phrases
    """
    re_num_patn = re.compile(RuleServiceNumber.num_patn)

    def __init__(self):
        super().__init__()
        super().set_logger(__name__)
        self.first_run_apply = True
        self.tokenizer = PhraseTokenizer('ToGetPhrases')

    def apply(self, stream_obj):
        """
            For each recognized phrase, apply tries to find it in given stream.
            Because a phrase may reside in two items in a stream, with punctuations between the two words of a phase,
            'apply' needs to find the first word firstly, then the second. After that, a check of the characters
            between them is necessary, if not only punctuations occur between them, the result is not a valid match.

            For example, we're searching phrase 'Base Address'
                ['some text 0', 'some text 1', 'some base text 1', 'Base-', 'Address', 'some text n' ....]
            1. 'base' in 'some base text 1' will be found firstly, then 'Address' too.
                However, ' text 1' and 'Base-' are between these the first word and second of a phrase. Failed.
            2. Start another round in
                [' text 1', 'Base-', 'Address', 'some text n' ....]
                'Base-' and 'Address' are found, and only '-' is between them. Succeed and build a phrase token.
        """
        if not isinstance(stream_obj, TextStream):
            return None
        if not isinstance(stream_obj.stream, list):
            raise TypeError('RulePhraseCollector need a stream with list type')

        if self.first_run_apply:
            self.tokenizer.recognize_phrases()
            self.first_run_apply = False

        self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': ' '*5})
        stream_obj.stream = self.tokenizer.tokenize_phrases(stream_obj.stream)
        self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': 'after'})

        return stream_obj.stream
