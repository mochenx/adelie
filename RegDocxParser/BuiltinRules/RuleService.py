#encoding=utf-8
__author__ = 'mochenx'

import re


def count_digits_in_text(text):
    n_cnt = 0
    for c in text:
        if re.match(r'\d+', c):
            n_cnt += 1
    return n_cnt


def get_match_hex_func(re_rule, prob_func):
    def _match_hex_high(text):
        m_rslt = re.compile(re_rule).match(text)
        if m_rslt is None:
            return False
        n_cnt = count_digits_in_text(m_rslt.group(1))
        probability = prob_func(n_cnt, len(text[1:]))
        return probability
    return _match_hex_high


class RuleServiceNumber(object):
    hex_nums = r'[\da-fA-F_]+'
    dec_nums = r'[1-9]\d*|0'

    # 32'hxxx
    vlog_base = r"'[bBoOdDhH]"
    vlog_num_patn = r"({0})?{1}{2}".format(dec_nums, vlog_base, hex_nums)

    # literals with pattern like 0x??? must be a hex number
    hex_patn_sure = r'0[xX]{0}'.format(hex_nums)
    # literals with pattern like 3af5 may be a hex number, and it matches dec number before hex,
    # so decimals are excluded before hand
    hex_patn_high = r'\d({0})'.format(hex_nums)
    # literals with pattern like af5, BE or xAe5 might be a hex number. It supposes a hex number MUST contain at least
    # ONE number, so 'BE' WILL NOT be recognized as a hex here, but be5 is a valid hex with 60% * 1/3 probability.
    hex_patn_prob = r'[xX]{0}'.format(hex_nums)
    hex_patn = r'({0})|({1})|({2})'.format(hex_patn_sure, hex_patn_high, hex_patn_prob)
    num_patn = r'({4})|({3})|({0})|({1})|({2})'.format(hex_patn_sure, hex_patn_high, hex_patn_prob,
                                                       dec_nums, vlog_num_patn)

    num_prob = {'HEX_SURE': (lambda e: 1 if re.compile(RuleServiceNumber.hex_patn_sure).match(e) else 0),
                'HEX_HIGH': get_match_hex_func(hex_patn_high, (lambda n, d: 0.5 + 0.5*n/d)),
                'HEX_PROB': get_match_hex_func(hex_patn_prob, (lambda n, d: 0.6 * n/d)),
                'VLOG_BASE': lambda e: (1 if re.compile(RuleServiceNumber.vlog_num_patn).match(e) else 0),
                'DEC_SURE': (lambda e: 1 if re.compile(RuleServiceNumber.dec_nums).match(e) else 0)
                }
    patn_order = ['VLOG_BASE', 'DEC_SURE', 'HEX_SURE', 'HEX_HIGH', 'HEX_PROB']

    is_hex = (lambda e: True if re.compile(RuleServiceNumber.hex_patn).match(e) else False)

    @staticmethod
    def get_num_probability(text):
        max_probability = 0
        for name in RuleServiceNumber.patn_order:
            calc_prob_func = RuleServiceNumber.num_prob[name]
            probability = calc_prob_func(text)
            if probability == 1:
                return probability
            max_probability = probability if max_probability < probability else max_probability

        return max_probability


class RuleServiceVarName(object):
    var_name = r'[_\d\w]+'
    re_var_name = re.compile(r'({0})'.format(var_name))
    re_break_sent_punct = re.compile(r'[\.!\?;,:\s]')
    re_del_punct = re.compile(r"[#$%&'\*\+-/<=>@\\^`|~\(\)\[\]\{\}]")
