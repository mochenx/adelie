# encoding = utf-8
__author__ = 'mochenx'

from RegDocxParser.StreamRule import StreamRule
from docx.oxml.ns import NamespacePrefixedTag
from RegDocxParser.Stream.TableStream import TableStream


class ParsedDocxTable(list):
    class Row(dict):
        def __init__(self, the_table_cell_belongs):
            self.in_table = the_table_cell_belongs

        def insert(self, column_index, cell_val):
            self[column_index] = cell_val

    def new_row(self):
        return ParsedDocxTable.Row(self)

    def append_row(self, row):
        self.append(row)

    def insert_cell(self, column_index, cell_val, row_index=-1):
        self[row_index][column_index] = cell_val

    @property
    def last_row(self):
        return self[-1]


class RuleTableReader(StreamRule):
    """
        A rule of reading & parsing tables in Docx file
    """
    @staticmethod
    def get_span_val_getter(tag_name, func_of_draw_val):
        """
            Return a function which gives off the value of the given tag in an element tree of OpenXML
        """
        prefixed_tag = NamespacePrefixedTag('w:{0}'.format(tag_name))

        def get_val_from_cell(cell):
            try:
                elem = cell._tc.find('.//{{{0}}}{1}'.format(prefixed_tag._ns_uri, tag_name))
            except (AttributeError, TypeError) as e:
                print('Current type of given Cell: {0!s}'.format(type(cell)))
                raise e
            val = func_of_draw_val(elem) if elem is not None else None
            return val

        return get_val_from_cell

    def __init__(self):
        """
        """
        super().set_logger(__name__)

    def apply(self, docx_stream):
        for a_stream in docx_stream:
            self.stream_apply(a_stream)

    def get_cell_val(self, cell, get_val_of_merged_cell):
        """
            Return current value of given cell for Non-merged cell or the first cell of merged(merged row) cells
            Return the value of the previous row, the value can be gotten from calling of get_val_of_merged_cell
        """
        get_str_in_cell = lambda cell: ' '.join([p.text for p in cell.paragraphs])

        # row_span variable stores the number of rows which are merged
        # Define function for retrieving row span(vMerge)
        get_row_span_val = self.get_span_val_getter(tag_name='vMerge', func_of_draw_val=lambda e: e.values())
        row_span = get_row_span_val(cell)
        if row_span is None:  # Non-merged cell
            cell_val = get_str_in_cell(cell)
        elif len(row_span) and row_span[0] == 'restart':  # The first cell in a merged cell
            cell_val = get_str_in_cell(cell)
        else:  # The following cells in a merged cell
            cell_val = get_val_of_merged_cell()
        return cell_val

    def fill_cells_of_a_row(self, row_in_docx_table, parsed_table):
        """
            Iterate all cells in given row, fetch value of each of them and fill parsed table with proper value
        """

        contents_of_a_row = parsed_table.new_row()

        # Define function for retrieving column span(gridSpan)
        get_column_span_val = self.get_span_val_getter(tag_name='gridSpan',
                                                       func_of_draw_val=lambda e: int(e.values()[0]))
        column_idx = 0
        for cell in row_in_docx_table.cells:
            contents_of_a_row[column_idx] = self.get_cell_val(cell, lambda: parsed_table.last_row[column_idx])
            # col_span variable stores the number of columns which are merged
            col_span = get_column_span_val(cell)
            # Skip column merged cells
            column_idx += col_span if col_span else 1

        return contents_of_a_row

    def stream_apply(self, stream_obj):
        """
            Scan table from Docx and return a ParsedDocxTable object.
            Actually, a list of hashes like the following:
            [
                {0: ['11'],                       3: ['12'], 4: ['13']},
                {0: ['21'],            2: ['22'], 3: ['23'],            5: ['25']},
                {0: ['31'], 1: ['32'],            3: ['33'], 4: ['34']},
                {0: ['41'], 1: ['32'],            3: ['43'], 4: ['44']},
                {0: ['51'], 1: ['32'],            3: ['53'], 4: ['54']}
            ]
        """
        if not isinstance(stream_obj, TableStream):
            return None

        parsed_table = ParsedDocxTable()
        for r_idx, row in enumerate(stream_obj.raw.rows):
            parsed_row = self.fill_cells_of_a_row(row, parsed_table)
            parsed_table.append_row(parsed_row)
        stream_obj.stream = parsed_table
        return parsed_table
