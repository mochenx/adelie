# encoding = utf-8
__author__ = 'mochenx'

import re
from RegDocxParser.StreamRule import StreamRule
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.BuiltinRules.RuleService import RuleServiceNumber
from RegDocxParser.BuiltinRules.RulePhrase import Phrase
from RegDocxParser.Stream import TextStream


class TokenAssignment(DocxToken):

    def __init__(self, lhs=None, op=None, rhs=None):
        self.lhs = lhs
        self.op = op
        self.rhs = rhs
        self.probability = {'lhs': 0, 'rhs': 0}

    def __str__(self):
        _str = (str(self.lhs) if self.lhs else '') + str(self.op) + str(self.rhs)
        return _str

    def __repr__(self):
        return '<<TokenAssignment[[{0} {1}:{rhs}]]>>'.format(self.op, self.rhs, **self.probability)


class RuleNumAssign(StreamRule):
    bracket = lambda e: r'({0})'.format(e)
    # hex_num = r'[a-fA-F0-9xX_]'
    op_pattern = r' *[:=] *'
    lhs_pattern = r' *\b\w+\b *'
    rhs_pattern = r' *({0})\b *'.format(RuleServiceNumber.num_patn)
    search_patn = bracket(lhs_pattern) + bracket(op_pattern) + rhs_pattern
    split_patn = bracket(op_pattern + rhs_pattern)
    re_search_patn = re.compile(search_patn)
    re_split_patn = re.compile(split_patn)

    def __init__(self, exe_collect=True):
        super().set_logger(__name__)
        self.exe_collect = exe_collect

    def apply(self, stream_obj):
        if not isinstance(stream_obj, TextStream):
            return None
        if not isinstance(stream_obj.stream, list):
            raise TypeError('RuleNumAssign need a stream with list type')

        if self.exe_collect:
            ret_lst = self.collect(stream_obj)
        else:
            ret_lst = self.build(stream_obj)

        return ret_lst

    def build(self, stream_obj):
        self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': 'before'})
        del_idx_q = []

        for i, t in enumerate(stream_obj.stream):
            if i == 0 or not isinstance(t, TokenAssignment):
                continue
            prev_tkn = stream_obj.stream[i - 1]
            if (isinstance(prev_tkn, Phrase) or
                isinstance(prev_tkn, str) and re.match(RuleNumAssign.lhs_pattern, prev_tkn)):
                stream_obj.stream[i].lhs = prev_tkn
                del_idx_q.append(i - 1)

        for i in sorted(del_idx_q, reverse=True):
            del stream_obj.stream[i]
        splited_t_lst = stream_obj.stream

        self.logger.debug(StreamRule.f_print_rule_rslt(splited_t_lst), extra={'source': 'after '})
        return splited_t_lst

    def collect(self, stream_obj):
        self.logger.debug(StreamRule.f_print_rule_rslt(stream_obj.stream), extra={'source': 'before'})
        splited_t_lst = []
        for i, t in enumerate(stream_obj.stream):
            if isinstance(t, DocxToken):
                splited_t_lst.append(t)
                continue

            remain_idx = 0
            # r'(\b\w+\b)'
            # r'([:=])'
            # r'( *(([1-9]\d*|0)?\'[bBoOdDhH][\da-fA-F_]+)|'
            #    r'([1-9]\d*|0)|(0[xX][\da-fA-F_]+)|'
            #    r'(\d([\da-fA-F_]+))|([xX][\da-fA-F_]+)\b)')
            for m_rslt in RuleNumAssign.re_search_patn.finditer(t):
                # For each phrase, find an assignment pattern

                _LHS, _OP, _RHS = m_rslt.group(1), m_rslt.group(2), m_rslt.group(3)
                # TODO: The following 'search' operation should be replaced with 'match' to r'[a-zA-Z_]\w+', because
                #       0x00 will pass the test in next line
                if not re.search(r'\w', _LHS):
                    # At least, there is some alphabets in LHS.
                    continue
                # String before token
                # splited_t_lst.append(t[remain_idx:m_rslt.end(1)])
                splited_t_lst.append(t[remain_idx:m_rslt.start(1)])
                splited_t_lst.append(_LHS.lstrip(' ').rstrip(' '))
                # Token
                _RHS = _RHS.lstrip(' ').rstrip(' ')
                _OP = _OP.lstrip(' ').rstrip(' ')
                new_tkn = TokenAssignment(op=_OP, rhs=_RHS)
                new_tkn.probability['rhs'] = RuleServiceNumber.get_num_probability(_RHS)
                splited_t_lst.append(new_tkn)
                remain_idx = m_rslt.end(3)

            # The remains
            if remain_idx < len(t) and not re.match(r'^\s+$', t[remain_idx:]):
                splited_t_lst.append(t[remain_idx:])

        stream_obj.stream = splited_t_lst
        self.logger.debug(StreamRule.f_print_rule_rslt(splited_t_lst), extra={'source': 'after '})
        return splited_t_lst

