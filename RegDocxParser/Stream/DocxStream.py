# encoding = utf-8
__author__ = 'mochenx'


class DocxStreamBase(object):
    @property
    def raw(self):
        return self._raw_data

    @raw.setter
    def raw(self, val):
        self._raw_data = val

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, val):
        self._score = val

    @property
    def stream(self):
        return self._stream

    @stream.setter
    def stream(self, val):
        self._stream = val

    def clone(self, only_raw=False):
        raise NotImplementedError('Method DocxStreamBase.clone must be implemented in subclass')


class DocxStream(DocxStreamBase, list):
    """
        A list which stores objects of TextStream or TableStream
    """
    def __init__(self, link=None):
        if link and isinstance(link, DocxStreamBase):
            for s in link:
                self.append(s)

    @property
    def raw(self):
        raw = []
        for s in self:
            raw.extend(s.raw)
        return raw

    @property
    def stream(self):
        stream = []
        for s in self:
            stream.extend(s.stream)
        return stream

    def append(self, p_object):
        """
        """
        if not isinstance(p_object, DocxStreamBase):
            raise TypeError('{0} only accept the same or subclass as argument in method append'.format(self.__class__))
        super(DocxStream, self).append(p_object)

    def clone(self, only_raw=False):
        cloned = DocxStream()
        for s in self:
            cloned.append(s.clone(only_raw))
        return cloned
