# encoding = utf-8
__author__ = 'mochenx'

from copy import deepcopy
from RegDocxParser.DocxToken import DocxToken
from RegDocxParser.Stream.DocxStream import DocxStreamBase


class TextStreamCursor:
    def __init__(self, index=None, position=None):
        self._index = index
        self._position = position

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, val):
        if not isinstance(val, int):
            raise TypeError('Property "position" only accepts int type value')
        self._position = val

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, val):
        if not isinstance(val, int):
            raise TypeError('Property "index" only accepts int type value')
        self._index = val

    def __str__(self):
        return 'Index:{0}, Position:{1}'.format(self.index, self.position)

    def __eq__(self, other):
        if not isinstance(other, TextStreamCursor):
            raise TypeError('The type of RHS object({0}) is different from TextStreamCursor'.format(type(other)))
        if self.index == other.index and self.position == other.position:
            return True
        else:
            return False


class TextStream(DocxStreamBase):
    """
        A list like this:
        ["OSB_ID=0x00", "Name: MAX_LINE_RATE", "Base Address: 0x0000", "offset: 0x01", "DisplayPort address: 0x00001 "]
    """
    def __init__(self, raw_data=None):
        if raw_data:
            self.raw = raw_data
            self._stream = raw_data.text
        else:
            self.raw = None
            self._stream = None

        self.score = 0

    def clone(self, only_raw=False):
        cloned_self = TextStream(self.raw)
        if not only_raw:
            cloned_self.stream = deepcopy(self.stream)
        return cloned_self

    def __len__(self):
        return len(self.stream)

    @property
    def stream(self):
        return self._stream

    @stream.setter
    def stream(self, val):
        if not isinstance(val, list):
            raise TypeError('Property "stream" in TextStream needs to be a list instead of {0}'.format(type(val)))
        self._stream = val

    def split_stream(self, start_with, end_with=None, end_before=None):
        """
            Split income stream list at the indicated point, and return three lists
            Structure of input stream with start index: 1, end index: 2, start position: 5 end position: 3
                ['some text 0', 'some text 1', 'some text 2', ...... ]
                                      _____     ____
            Return:
                before:  ['some text 0', 'some ']
                in_range: ['text 1', 'some']
                after:  [' text 2', ...... ]
        """
        before = []
        in_range = []
        after = []
        if end_with is None and end_before is None:
            return ValueError('One of end_with or end_before should be included in arguments of split_stream')

        if end_with is not None:
            end_pos = end_with.position + 1
            end_index = end_with.index
        elif end_before is not None:
            end_pos = end_before.position
            end_index = end_before.index

        for i, t in enumerate(self.stream):
            if i < start_with.index:
                before.append(t)
                continue
            elif i > end_index:
                after.append(t)
                continue
            elif isinstance(t, DocxToken):
                continue

            if i == start_with.index and start_with.index == end_index:
                if start_with.position > 0:
                    before.append(t[0: start_with.position])
                in_range.append(t[start_with.position: end_pos])
                if end_pos < len(t):
                    after.append(t[end_pos:])
            elif i == start_with.index:
                if start_with.position > 0:
                    before.append(t[0: start_with.position])
                in_range.append(t[start_with.position:])
            elif i == end_index:
                if end_with is not None or end_pos != 0:
                    in_range.append(t[: end_pos])
                if end_pos < len(t):
                    after.append(t[end_pos:])
            else:  # start_cursor.index < i < end_cursor.index
                in_range.append(t)

        return before, in_range, after