# encoding = utf-8
from RegDocxParser.Stream.DocxStream import DocxStreamBase

__author__ = 'mochenx'

from copy import deepcopy


class TableStream(DocxStreamBase):
    def __init__(self, raw_data=None):
        if raw_data:
            self.raw = raw_data
        else:
            self.raw = None

        self.stream = None
        self.score = 0

    def clone(self, only_raw=False):
        cloned_self = TableStream(self.raw)
        if not only_raw:
            cloned_self.stream = deepcopy(self.stream)
        return cloned_self