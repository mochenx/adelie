# Draft for Tables in docx
grid_span = NamespacePrefixedTag('w:gridSpan')
cell0 = tab.raw.cell(0, 1)
cell_span = cell0._tc.find('.//{%s}gridSpan' % grid_span._ns_uri)

span = 1
if cell_span:
    span = cell_span.values()[0]


>>> cell0 = tab.raw.cell(3, 1)
>>> span = cell0._tc.find('.//{%s}vMerge' % grid_span._ns_uri)
>>> span.values()

>>> span.values()
['restart']

Or

>>> span.values()
[]


# Algorithm of grouping the nearest numbers in a list
d = [1915, 1894, 1893, 1537, 1489, 398, 356, 21, 5, 3, 3, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

d.sort()

diff = [d[i+1]-d[i] for i in range(len(d)-1)]
avg = sum(diff) / len(diff)

m = [[d[0]]]

for x in d[1:]:
    if x - m[-1][-1] < avg:
        m[-1].append(x)
    else:
        m.append([x])


print(m)

