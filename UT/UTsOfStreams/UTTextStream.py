# encoding = utf-8
__author__ = 'mochenx'

from RegDocxParser.Stream.TextStream import TextStreamCursor, TextStream
from RegDocxParser.DocxToken import DocxToken

import unittest


class UTTextStreamCursor(unittest.TestCase):
    def setUp(self):
        self.default_dut = TextStreamCursor(123, 456)

    def check_default(self, index, position):
        self.assertEqual(self.default_dut.index, index)
        self.assertEqual(self.default_dut.position, position)

    def test_getter(self):
        self.check_default(123, 456)
        print(self.default_dut)

    def test_setter(self):
        self.default_dut.index = -1
        self.default_dut.position = -1
        self.check_default(-1, -1)
        print(self.default_dut)

    def test_setter_type_err(self):
        def set_index():
            self.default_dut.index = '0'

        def set_position():
            self.default_dut.position = float(100)
        print(self.default_dut)

        self.assertRaises(TypeError, set_index)
        self.assertRaises(TypeError, set_position)
        self.check_default(123, 456)


class UTTextStream(unittest.TestCase):
    class FakeDocxText:
        def __init__(self, text=None):
            if text is None:
                self.text = 'account,acid,across,act,addition,adjustment,advertisement,after,' \
                            'again,against,agreement,air,all,almost,among,amount,amusement,and,' \
                            'angle,angry,animal,answer'
            else:
                self.text = text

    class FakeTokens(DocxToken):
        def __init__(self, name):
            self.name = name

        def __str__(self):
            return self.name

    def setUp(self):
        self.raw = UTTextStream.FakeDocxText()
        self.expt_set = set(self.raw.text.split(','))
        self.default_dut = TextStream(raw_data=self.raw)

    def test_stream_getter(self):
        print(self.default_dut.stream)
        self.assertEqual(self.default_dut.stream, self.raw.text)

    def test_stream_setter(self):
        self.default_dut.stream = self.raw.text.split(',')
        print(self.default_dut.stream)
        self.assertEqual(set(self.default_dut.stream), self.expt_set)

    def test_stream_setter_type_err(self):
        def set_stream():
            self.default_dut.stream = ''
        self.assertRaises(TypeError, set_stream)
        print(self.default_dut.stream)

    def test_stream_len(self):
        expect_len = len(self.raw.text.split(','))
        self.default_dut.stream = self.raw.text.split(',')
        print(self.default_dut.stream)
        self.assertEqual(len(self.default_dut), expect_len)

    def test_stream_clone_raw_only(self):
        """
            1. Modify attribute 'stream' in original DUT
            2. Clone DUT with 'only_raw' argument
            3. Modify cloned DUT
            4. Check
        """
        orig_text = self.default_dut.raw.text
        expect_raw = 'Shared'
        # 1. Modify attribute 'stream' in original DUT
        self.default_dut.stream = self.raw.text.split(',')
        # 2. Clone DUT with 'only_raw' argument
        cloned_dut = self.default_dut.clone(only_raw=True)
        # 3. Modify cloned DUT
        cloned_dut.raw.text = expect_raw
        self.assertEqual(cloned_dut.raw, self.default_dut.raw)
        self.assertEqual(self.default_dut.raw.text, expect_raw)
        self.assertEqual(cloned_dut.stream, orig_text)
        print(cloned_dut.stream)

    def test_stream_clone(self):
        """
            1. Modify attribute 'stream' in original DUT
            2. Clone DUT
            3. Modify cloned DUT
            4. Check
        """
        expect_raw = 'Shared'
        # 1. Modify attribute 'stream' in original DUT
        self.default_dut.stream = self.raw.text.split(',')
        # 2. Clone DUT with 'only_raw' argument
        cloned_dut = self.default_dut.clone()
        # 3. Modify cloned DUT
        cloned_dut.raw.text = expect_raw
        self.assertEqual(cloned_dut.raw, self.default_dut.raw)
        self.assertEqual(self.default_dut.raw.text, expect_raw)
        self.assertEqual(set(cloned_dut.stream), self.expt_set)
        print(cloned_dut.stream)

    def check_word(self, word, index):
        _bf, sel_word, _af = self.default_dut.split_stream(start_with=TextStreamCursor(index=index, position=0),
                                                           end_with=TextStreamCursor(index=index, position=(len(word)) - 1))
        self.assertEqual(len(_bf), index)
        self.assertEqual(len(_af), len(self.expt_set) - index - 1)
        self.assertEqual(len(sel_word), 1)
        self.assertEqual(sel_word[0], word)
        print(sel_word[0])

    def check_words(self, s_index, e_index, **kwargs):
        start_position = 0
        split_start = 0
        if 'start_position' in kwargs:
            start_position = kwargs['start_position']
            del kwargs['start_position']
            split_start = 1 if start_position > 0 else 0

        split_end = 0
        end_position = (len(kwargs[str(e_index)]) - 1)
        # Attention Please, when end_position is set to -1, it means the word at the end position will NOT be cut
        #   into two parts, so no more item is created in list. Caller MUST use -1 instead the length of word
        #   when whole word is selected, for the reason that there's no way to find that
        #   if or not the last one will be cut beforehand.
        if 'end_position' in kwargs:
            if kwargs['end_position'] != -1:
                split_end = 1
                end_position = kwargs['end_position']
            del kwargs['end_position']

        _bf, sel_words, _af = self.default_dut.split_stream(start_with=TextStreamCursor(index=s_index,
                                                                                        position=start_position),
                                                            end_with=TextStreamCursor(index=e_index,
                                                                                      position=end_position))
        self.assertEqual(len(_bf), s_index + split_start)
        self.assertEqual(len(_af), len(self.expt_set) - e_index - 1 + split_end)
        self.assertEqual(len(sel_words), len(kwargs))
        for idx, word in sorted(kwargs.items(), key=lambda e: e[0]):
            self.assertEqual(word, sel_words[int(idx) - s_index])
            print('{0}: {1}'.format(idx, word))

    def check_words_end_before_something(self, s_index, e_index, **kwargs):
        start_position = 0
        split_start = 0
        if 'start_position' in kwargs:
            start_position = kwargs['start_position']
            del kwargs['start_position']
            split_start = 1 if start_position > 0 else 0

        end_position = kwargs['end_position']
        split_end = 0
        del kwargs['end_position']

        _bf, sel_words, _af = self.default_dut.split_stream(start_with=TextStreamCursor(index=s_index,
                                                                                        position=start_position),
                                                            end_before=TextStreamCursor(index=e_index,
                                                                                        position=end_position))
        self.assertEqual(len(_bf), s_index + split_start)
        self.assertEqual(len(_af), len(self.expt_set) - e_index - split_end)
        self.assertEqual(len(sel_words), len(kwargs))
        for idx, word in sorted(kwargs.items(), key=lambda e: e[0]):
            self.assertEqual(word, sel_words[int(idx) - s_index])
            print('{0}: {1}'.format(idx, word))

    def test_stream_split_get_word(self):
        """
            advertisement: index 6, position 0 ~ position 12
            after: index 7, position 0 ~ position 4
            answer: index -1, position 0 ~ position 5
        """
        self.default_dut.stream = self.raw.text.split(',')
        self.check_word('advertisement', index=6)
        self.check_word('after', index=7)
        self.check_word('answer', index=len(self.expt_set)-1)

    def test_stream_split_phrase(self):
        """
        end_with mode:
            almost, among: index 13 ~ 14
            amusement,and angle: index 16 ~ 18
            angle,angry,animal,answer: index 18 ~ 21
        Round 2, end_before mode:
            almost, among: index 13 ~ 15
            amusement,and angle: index 16 ~ 19
            angle,angry,animal,answer: index 18 ~ 22
        """
        self.default_dut.stream = self.raw.text.split(',')
        self.check_words(13, 14, **{'13': 'almost', '14': 'among'})
        self.check_words(16, 18, **{'16': 'amusement', '17': 'and', '18': 'angle'})
        self.check_words(18, 21, **{'18': 'angle', '19': 'angry', '20': 'animal', '21': 'answer'})
        print('==================== Round 2 ====================')
        self.check_words_end_before_something(13, 15, **{'end_position': 0,
                                                         '13': 'almost', '14': 'among'})
        self.check_words_end_before_something(16, 19, **{'end_position': 0,
                                                         '16': 'amusement', '17': 'and', '18': 'angle'})
        self.check_words_end_before_something(18, 22, **{'end_position': 0,
                                                         '18': 'angle', '19': 'angry', '20': 'animal', '21': 'answer'})

    def test_stream_split_cut_words(self):
        """
        end_with mode:
            almost: index 13 ~ 14, position 3 ~ position 3
            angle,angry,animal,answer: index 18 ~ 21, position 4 ~ position 0
        end_before mode:
            almost: index 13 ~ 14, position 3 ~ position 4
            angle,angry,animal,answer: index 18 ~ 21, position 4 ~ position 1
        """
        self.default_dut.stream = self.raw.text.split(',')
        self.check_words(13, 14, **{'start_position': 3, 'end_position': 3,
                                    '13': 'ost', '14': 'amon'})
        self.check_words(18, 21, **{'start_position': 4, 'end_position': 0,
                                    '18': 'e', '19': 'angry', '20': 'animal', '21': 'a'})
        print('==================== Round 2 ====================')
        self.check_words_end_before_something(13, 14, **{'start_position': 3, 'end_position': 4,
                                                         '13': 'ost', '14': 'amon'})
        self.check_words_end_before_something(18, 21, **{'start_position': 4, 'end_position': 1,
                                                         '18': 'e', '19': 'angry', '20': 'animal', '21': 'a'})

    def test_stream_split_cut_single_word(self):
        """
        end_with mode:
            addition: index 4, position 0 ~ position 0
            adjustment: index 5, position 2 ~ position 2
            account: index 0, position 0 ~ position 7
            across: index 2, position 2 ~ position 3
        end_before mode:
            addition: index 4, position 0 ~ position 1
            adjustment: index 5, position 2 ~ position 3
            account: index 0 ~ index 1, position 0 ~ position 0
            across: index 2, position 2 ~ position 4
        """
        self.default_dut.stream = self.raw.text.split(',')
        self.check_words(4, 4, **{'start_position': 0, 'end_position': 0,
                                  '4': 'a'})
        self.check_words(5, 5, **{'start_position': 2, 'end_position': 2,
                                  '5': 'j'})
        self.check_words(0, 0, **{'start_position': 0, 'end_position': -1,  # The special requirement by 'check_words'
                                  '0': 'account'})
        self.check_words(2, 2, **{'start_position': 2, 'end_position': 3, '2': 'ro'})
        print('==================== Round 2 ====================')
        self.check_words_end_before_something(4, 4, **{'start_position': 0, 'end_position': 1,
                                                       '4': 'a'})
        self.check_words_end_before_something(5, 5, **{'start_position': 2, 'end_position': 3,
                                                       '5': 'j'})
        self.check_words_end_before_something(0, 1, **{'start_position': 0, 'end_position': 0,
                                                       '0': 'account'})
        self.check_words_end_before_something(2, 2, **{'start_position': 2, 'end_position': 4, '2': 'ro'})
