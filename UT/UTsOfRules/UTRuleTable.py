# encoding = utf-8
__author__ = 'mochenx'

import unittest
import os
from docx.table import Table
from docx.text import Paragraph

from RegDocxParser.DocxParser import DocxParser
from RegDocxParser.BuiltinRules.RuleTableReader import RuleTableReader
from RegDocxParser.BuiltinRules.RuleTypTabParser import RuleTypTabTitleBuilder, RuleTypTabTitleCollector
from RegDocxParser.Stream import DocxStream, TextStream
from RegDocxParser.Stream.TableStream import TableStream


class UTRuleTable(unittest.TestCase):
    def prepare(self, fname):
        self.reg_docx = DocxParser(docx_file_name=fname)

        self.docx_stream = DocxStream()
        for o in self.reg_docx:
            if isinstance(o, Paragraph) and o.text:
                self.docx_stream.append(TextStream(raw_data=o))
            elif isinstance(o, Table):
                self.docx_stream.append(TableStream(raw_data=o))
        self.rules = [RuleTableReader(), RuleTypTabTitleCollector(), RuleTypTabTitleBuilder()]
        RuleTypTabTitleBuilder.preset_categories['Default Value'] = 'Default'
        RuleTypTabTitleBuilder.preset_categories['Description( R Is Lut Value)'] = 'Description'
        RuleTypTabTitleBuilder.preset_categories['Reg Addr'] = 'Address'
        RuleTypTabTitleBuilder.preset_categories['Offset Address'] = 'Address'
        RuleTypTabTitleBuilder.preset_categories['Bit(s)'] = 'Bit'

    def setUp(self):
        self.prepare(fname=os.path.join('tc_docx', 'Table.docx'))

    def test_table_tc1(self):
        """
             ------------------------------
            | 11           | 12 | 13      |
            | 21      | 22 | 23      | 24 |
            | 31 | 32      | 33 | 34      |
            | 41 |         | 43 | 44      |
             ------------------------------
        """
        expt_table = [
            {0: '11',                       3: '12', 4: '13'},
            {0: '21',            2: '22', 3: '23',          5: '25'},
            {0: '31', 1: '32',            3: '33', 4: '34'},
            {0: '41', 1: '32',            3: '43', 4: '44'},
            {0: '51', 1: '32',            3: '53', 4: '54'}
        ]
        tc_stream = self.docx_stream.clone()
        cnt = 0
        print('-'*30)
        self.rules[0].apply(tc_stream)
        for s in tc_stream:
            lst = s.stream
            for r, row in enumerate(lst):
                for k, v in row.items():
                    self.assertEqual(int(v), int(expt_table[r][k]))

    def test_table_title_words(self):
        self.prepare(fname=os.path.join('tc_docx', 'MassTest.docx'))

        tc_stream = self.docx_stream.clone()
        print('-'*30)
        for i in range(3):
            self.rules[i].apply(tc_stream)

