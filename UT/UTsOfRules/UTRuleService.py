# encoding=utf-8
__author__ = 'mochenx'

import unittest
import re
import os
from RegDocxParser.BuiltinRules.RuleService import RuleServiceNumber


class UTRuleService(unittest.TestCase):
    def test_sure(self):
        stimu = ["32'HA00", "'h00", "16'd123", "'D100", "'b1000_0001", "4'B1010", "8'o34", "'O12",
                 "128", "0x1A_2B", "0XABCDEF012345", "0xabcdef012345", "0xabcd_ef01_2345", "1", "10", "012"
                 ]
        for c, s in enumerate(stimu):
            print('Test {1}: {0}'.format(s, c))
            self.assertEqual(RuleServiceNumber.get_num_probability(s), 1)
            self.assertIsNotNone(re.match(RuleServiceNumber.num_patn, s))

