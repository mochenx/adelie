# encoding = utf-8
__author__ = 'mochenx'

from docx.table import Table
from docx.text import Paragraph

import unittest
import os
from RegDocxParser.DocxParser import DocxParser
from RegDocxParser.BuiltinRules.RuleLineSplitter import RuleLineSplitter
from RegDocxParser.Stream import DocxStream, TextStream
from RegDocxParser.Stream.TableStream import TableStream


class UTRuleLineSpliter(unittest.TestCase):
    def prepare(self, fname):
        self.reg_docx = DocxParser(docx_file_name=fname)

        self.docx_stream = DocxStream()
        for o in self.reg_docx:
            if isinstance(o, Paragraph) and o.text:
                self.docx_stream.append(TextStream(raw_data=o))
            elif isinstance(o, Table):
                self.docx_stream.append(TableStream(raw_data=o))
        self.rule = RuleLineSplitter()

    def test_reg_docx_tc1(self):
        """
            Test of parsing tc_docx/CntxtOSB_basic1.docx
        """
        expt_val = {18: '18---Santa Clara, CA 95054, USA',
                    }
        self.prepare(fname=os.path.join('tc_docx', 'CntxtOSB_basic1.docx'))
        tc_stream = self.docx_stream.clone()
        cnt = 0
        print('-'*30)
        for s in tc_stream:
            lst = self.rule.apply(s)
            if lst:
                print(s.raw.text)
                print('====>')
                s_obsv = '{0}---{1}'.format(cnt, '|'.join([s if isinstance(s, str) else str(s) for s in lst]))
                print(s_obsv)
                if cnt in expt_val:
                    self.assertEqual(expt_val[cnt], s_obsv)
                print('-'*30)
                cnt += 1
            else:
                self.assertTrue(isinstance(s, TableStream))

    def test_reg_docx_tc2(self):
        """
            Test of parsing tc_docx/MassTest.docx
        """
        expt_val = {100: '100---OSB_ID=0x00|    |'
                    'Name: SYMBOL_ERROR_COUNT_LANE0_1 Base Address: 0x0002 offset: 0x11 DisplayPort Address: 0x00211',
                    200: '200---OSB_ID=0x00|    |'
                    'Name: TEST_RESPONSE Base Address: 0x0002 offset: 0x60|  |'
                    'DisplayPort Address: 0x00260',
                    299: '299---Table|   |EDP_BL_BRIGHTNESS_MSB'
                    }
        self.prepare(fname=os.path.join('tc_docx', 'MassTest.docx'))
        tc_stream = self.docx_stream.clone()
        cnt = 0
        print('-'*30)
        for s in tc_stream:
            lst = self.rule.apply(s)
            if lst:
                s_obsv = '{0}---{1}'.format(cnt, '|'.join([s if isinstance(s, str) else str(s) for s in lst]))
                if cnt in expt_val:
                    self.assertEqual(expt_val[cnt], s_obsv)
                if cnt % 100 == 0:
                    print(s.raw.text)
                    print('====>')
                    print(s_obsv)
                    print('-'*30)
                cnt += 1
            else:
                self.assertTrue(isinstance(s, TableStream))

