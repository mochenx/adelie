# encoding = utf-8
from RegDocxParser.NLP.ColumnTitle import ColumnTitle

__author__ = 'mochenx'

import unittest
from RegDocxParser.BuiltinRules.RuleTypTabParser import RuleTypTabTitleBuilder
from RegDocxParser.Tokens.Category import Category


class UTColumnTitles(unittest.TestCase):
    def setUp(self):
        pass

    def test_token_column_titles(self):
        klasses = [Category, ColumnTitle]
        names = ['Name0', 'Name1', 'Name0', 'Name2']

        for klass in klasses:
            title_objs = {}
            for name in names:
                new_obj = klass.get(name)
                if name in title_objs:
                    self.assertEqual(title_objs[name], new_obj)
                else:
                    title_objs[name] = new_obj
            self.assertEqual(len(title_objs), 3)

    def test_build_categories_from(self):
        phrases_with_freq = [('A Phrase1', 100), ('A Phrase2', 200), ('A Phrase3', 300), ('XXX', 4)]

        Category.clear()
        all_category_names = RuleTypTabTitleBuilder.build_categories_from(phrases_with_freq)
        self.assertEqual(len(all_category_names), len(phrases_with_freq))
        chk_phrases = phrases_with_freq[:]
        for i, phrase_freq in enumerate(phrases_with_freq):
            phrase = phrase_freq[0]
            self.assertIn(member=phrase, container=all_category_names)
            chk_phrases[i] = True

        for phrase in chk_phrases:
            self.assertTrue(phrase)

    def test_similarity_match(self):
        recognized_names = ['Name', 'Type', 'Description', 'Default Values', 'Bit']
        unrecognized_names = [
            'Term', 'Author', 'Description( B Is Lut Value)',
            'Default Value(32t)', 'Description( R Is Lut Value)',
            'Default Value(39t)', 'De Value', 'Comments',
            'Rev #', 'Date', 'Description( G Is Lut Value)',
            'Byte\\bit', 'Offset Address', 'Reg Addr',
            'Default Value', 'Address', 'Bit(s)', 'Def Value'
        ]
        expt = set(['Default Value(32t)', 'Default Values', 'Default Value', 'Default Value(39t)'])
        recognized_names = [ColumnTitle.get(phrase) for phrase in recognized_names]
        unrecognized_names = [ColumnTitle.get(phrase) for phrase in unrecognized_names]
        grp_majority_n_minority = RuleTypTabTitleBuilder.get_similar_groups(x_names=unrecognized_names,
                                                                            y_names=recognized_names)
        print(str(grp_majority_n_minority))
        obsv = set(str(grp_majority_n_minority).split(','))
        self.assertEqual(obsv, expt)

    def test_refill_similar_categories(self):
        recognized_names = ['Name', 'Type', 'Description', 'Default Values', 'Bit']
        unrecognized_names = [
            'Term', 'Author', 'Description( B Is Lut Value)',
            'Default Value(32t)', 'Description( R Is Lut Value)',
            'Default Value(39t)', 'De Value', 'Comments',
            'Rev #', 'Date', 'Description( G Is Lut Value)',
            'Byte\\bit', 'Offset Address', 'Reg Addr',
            'Default Value', 'Address', 'Bit(s)', 'Def Value'
        ]
        expt = Category.get('Default Values')
        rule_tab_parser = RuleTypTabTitleBuilder()

        recognized_names = [ColumnTitle.get(phrase) for phrase in recognized_names]
        for title in recognized_names:
            title.token = str(title)
        unrecognized_names = [ColumnTitle.get(phrase) for phrase in unrecognized_names]
        grp_majority_n_minority = rule_tab_parser.get_similar_groups(x_names=unrecognized_names,
                                                                     y_names=recognized_names)
        rule_tab_parser.refill_similar_categories(grp_majority_n_minority, None)
        for group in grp_majority_n_minority.groups:
            for label in group:
                print('Label:{0!s} with Token:{1!s}'.format(label, label.token))
                self.assertEqual(label.token, expt)
