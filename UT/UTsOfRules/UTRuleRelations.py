# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.StreamRule import StreamRule


class RuleInUT(StreamRule):

    def __init__(self, rule_name):
        """
            'split_space_num' or more spaces are considered as a line break
        """
        self._rule_name = rule_name
        self.nothing = ""

    def apply(self, text):
        return text


class UTRuleRelations(unittest.TestCase):
    get_expr = lambda e: ' '.join([str(r) for r in e])

    def check(self, obsv_paths, expt_paths):
        for p in obsv_paths:
            obsv_p = tuple(p)
            if obsv_p in expt_paths:
                self.assertTrue(expt_paths[obsv_p])
                del expt_paths[obsv_p]
                print(' Pass: {0}'.format(UTRuleRelations.get_expr(obsv_p)))
        self.assertEqual(len(list(expt_paths.keys())), 0)

    def _test_build_tree(self, rules):
        # Build parsing tree
        parsed_infix_rule = StreamRule.build_infix_rule(rules)
        postfix = StreamRule.infix_to_postfix(parsed_infix_rule)
        pt = StreamRule.postfix_to_parsingtree(postfix)
        obsv_paths = pt.path

        print(UTRuleRelations.get_expr(parsed_infix_rule))
        print(UTRuleRelations.get_expr(postfix))
        return obsv_paths

    def test_build_infix_rule_tc0(self):
        """
        Rules:
           RuleInUT_0
        Paths:
           RuleInUT_0
        """
        # Perpare rules
        r0 = RuleInUT('RuleInUT_0')
        rules = [r0]
        expt_paths = {(r0, ): True}

        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)

    def test_build_infix_rule_tc1(self):
        """
        Rules:
           (RuleInUT_0.(RuleInUT_1|RuleInUT_2).RuleInUT_3)
        Paths:
           (RuleInUT_0.RuleInUT_1.RuleInUT_3)
           (RuleInUT_0.RuleInUT_2.RuleInUT_3)
        """
        # Perpare rules
        r0 = RuleInUT('RuleInUT_0')
        r1 = RuleInUT('RuleInUT_1')
        r2 = RuleInUT('RuleInUT_2')
        r3 = RuleInUT('RuleInUT_3')
        rules = [r0, set([r1, r2]), r3]
        expt_paths = {(r0, r1, r3): True, (r0, r2, r3): True}

        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)

    def test_build_infix_rule_tc2(self):
        """
        Rules:
           (RuleInUT_0.((RuleInUT_131.RuleInUT_132)|RuleInUT_11|RuleInUT_12).(RuleInUT_21.RuleInUT_22))
        Paths:
           (RuleInUT_0.RuleInUT_131.RuleInUT_132.RuleInUT_21.RuleInUT_22)
           (RuleInUT_0.RuleInUT_11.RuleInUT_21.RuleInUT_22)
           (RuleInUT_0.RuleInUT_12.RuleInUT_21.RuleInUT_22)
        """
        # Perpare rules
        r0 = RuleInUT('RuleInUT_0')
        r11 = RuleInUT('RuleInUT_11')
        r12 = RuleInUT('RuleInUT_12')
        r131 = RuleInUT('RuleInUT_131')
        r132 = RuleInUT('RuleInUT_132')
        r21 = RuleInUT('RuleInUT_21')
        r22 = RuleInUT('RuleInUT_22')

        rules = (r0,
                 set((r11, r12,
                      (r131, r132))),
                 [r21, r22])
        expt_paths = {(r0, r131, r132, r21, r22): True, (r0, r11, r21, r22): True, (r0, r12, r21, r22): True}

        # Build parsing tree
        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)

    def test_build_infix_rule_tc3(self):
        """
        Rules:
            ((RuleInUT_11.(RuleInUT_121|RuleInUT_122).RuleInUT_13)|(RuleInUT_22|RuleInUT_21))
        Paths:
            RuleInUT_11.RuleInUT_121.RuleInUT_13
            RuleInUT_11.RuleInUT_122.RuleInUT_13
            RuleInUT_22
            RuleInUT_21
        """
        # Perpare rules
        r11 = RuleInUT('RuleInUT_11')
        r121 = RuleInUT('RuleInUT_121')
        r122 = RuleInUT('RuleInUT_122')
        r13 = RuleInUT('RuleInUT_13')
        r21 = RuleInUT('RuleInUT_21')
        r22 = RuleInUT('RuleInUT_22')

        rules = set((
            (r11, frozenset((r121, r122)),
             r13),
            frozenset((r21, r22))
        ))
        expt_paths = {(r11, r121, r13): True, (r11, r122, r13): True, (r22, ): True, (r21, ): True}

        # Build parsing tree
        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)

    def test_build_infix_rule_tc4(self):
        """
        Rules:
            (RuleInUT_1|RuleInUT_3|RuleInUT_2|RuleInUT_0)
        Paths:
            RuleInUT_1
            RuleInUT_3
            RuleInUT_2
            RuleInUT_0
        """
        # Perpare rules
        r0 = RuleInUT('RuleInUT_0')
        r1 = RuleInUT('RuleInUT_1')
        r2 = RuleInUT('RuleInUT_2')
        r3 = RuleInUT('RuleInUT_3')

        rules = frozenset((r0, r1, r2, r3))
        expt_paths = {(r0, ): True, (r1, ): True, (r2, ): True, (r3, ): True}

        # Build parsing tree
        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)

    def test_build_infix_rule_tc5(self):
        """
        Rules:
            (RuleInUT_1.RuleInUT_3.RuleInUT_2.RuleInUT_0)
        Paths:
            RuleInUT_1 RuleInUT_3 RuleInUT_2 RuleInUT_0
        """
        # Perpare rules
        r0 = RuleInUT('RuleInUT_0')
        r1 = RuleInUT('RuleInUT_1')
        r2 = RuleInUT('RuleInUT_2')
        r3 = RuleInUT('RuleInUT_3')

        rules = (r0, r1, r2, r3)
        expt_paths = {(r0, r1, r2, r3): True}

        # Build parsing tree
        obsv_paths = self._test_build_tree(rules)

        # Check parsing tree
        self.check(obsv_paths, expt_paths)
