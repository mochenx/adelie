# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.BuiltinRules.RuleHDLRange import RuleHDLRange
import os
from docx.table import Table
from docx.text import Paragraph

from RegDocxParser.DocxParser import DocxParser
from RegDocxParser.BuiltinRules.RuleLineSplitter import RuleLineSplitter
from RegDocxParser.Stream import DocxStream, TextStream
from RegDocxParser.Stream.TableStream import TableStream


class UTRuleHDL(unittest.TestCase):
    def prepare(self, fname):
        self.reg_docx = DocxParser(docx_file_name=fname)

        self.docx_stream = DocxStream()
        for o in self.reg_docx:
            if isinstance(o, Paragraph) and o.text:
                self.docx_stream.append(TextStream(raw_data=o))
            elif isinstance(o, Table):
                self.docx_stream.append(TableStream(raw_data=o))
        self.rules = [RuleLineSplitter(), RuleHDLRange()]

    def setUp(self):
        self.prepare(fname=os.path.join('tc_docx', 'MassTest.docx'))

    def test_range_tc1(self):
        # expt_val = {100: '100---OSB_ID=0x00|<<TokenSoftBreak[[    ]]>>|'}
        expt_val = {}

        tc_stream = self.docx_stream.clone()
        cnt = 0
        print('-'*30)
        for s in tc_stream:
            lst = self.rules[0].apply(s)

        for s in tc_stream:
            lst = self.rules[1].apply(s)
            if lst:
                s_obsv = '{0}---{1}'.format(cnt, '|'.join([s if isinstance(s, str) else str(s) for s in lst]))
                if cnt in expt_val:
                    self.assertEqual(expt_val[cnt], s_obsv)
                if cnt % 100 == 0:
                    print(s.raw.text)
                    print('====>')
                    print(s_obsv)
                    print('-'*30)
                cnt += 1
            else:
                self.assertTrue(isinstance(s, TableStream))
