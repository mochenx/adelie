# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.NLP.LabelSimilarity import SimilarLabel, SimilarLabelsGroups, LabelSimilarityMatrix
from RegDocxParser.NLP.ColumnTitle import ColumnTitle


class UTLabelSimilarity(unittest.TestCase):
    def setUp(self):
        self.one_group_tuple_stimu = [('Def Value', 'Default Value'),
                                      ('Def Value', 'De Value'),
                                      ('Default Value(39t)', 'Default Value'),
                                      ('Default Value(32t)', 'Default Value'),
                                      ('Default Value(32t)', 'Default Value(39t)')]
        self.two_group_tuples_stimu = [('Def Value', 'De Value'),
                                       ('Default Value(39t)', 'Default Value'),
                                       ('Default Value(32t)', 'Default Value'),
                                       ('Default Value(32t)', 'Default Value(39t)'),
                                       ('Def Value', 'Default Value'),
                                       ('Description( G Is Lut Value)', 'Description( R Is Lut Value)'),
                                       ('Description( G Is Lut Value)', 'Description( B Is Lut Value)'),
                                       ('Description( B Is Lut Value)', 'Description( R Is Lut Value)')]

    @staticmethod
    def label_matrix_tc(x, y, checker):
        dut = LabelSimilarityMatrix(y=y, x=x)
        dut.fill_edit_distances()
        for y, row in dut.items():
            for x, v in row.items():
                checker(x, y, v)
        return dut

    def test_label_matrix_tc0(self):
        """
            All edit_distances of two title arrays will be ZERO or ONE
        """
        titles = ['Title0', 'Title1', 'Title2', 'TitleX']

        def checker(x, y, v):
            self.assertIn(member=y, container=titles)
            self.assertIn(member=x, container=titles)
            if y == x:
                self.assertEqual(v, 0)
            else:
                self.assertEqual(v, 1/6)
        self.label_matrix_tc(y=titles, x=titles[:], checker=checker)

    def test_label_matrix_tc1(self):
        title_y = ['Title0', 'Title1', 'Title2', 'TitleX']
        title_x = [ColumnTitle.get(t) for t in title_y]

        def checker(x, y, v):
            self.assertIn(member=y, container=title_y)
            self.assertIn(member=x, container=title_x)
            if y == str(x):
                self.assertEqual(v, 0)
            else:
                self.assertEqual(v, 1/6)
        self.label_matrix_tc(y=title_y, x=title_x, checker=checker)

    def test_label_matrix_tc2(self):
        title_y = ['incentive', 'bias', 'garrison', 'pancreas', 'vigilant']
        title_x = ['incontive', 'suffrage', 'intuitive', 'ideology', 'insurrection', 'empiricism']

        def checker(x, y, v):
            self.assertIn(member=y, container=title_y)
            self.assertIn(member=x, container=title_x)
            try:
                if y == 'incentive'and x == 'incontive':
                    self.assertEqual(v, 1/len('incontive'))
                elif x == 'incontive':
                    self.assertGreater(v, 1/len(x))
                else:
                    self.assertEqual(v, float('inf'))
            except AssertionError as e:
                print('x={0}, y={1}, value:{2}'.format(x, y, v))
                raise e
        title_matrix = self.label_matrix_tc(y=title_y, x=title_x, checker=checker)
        print(title_matrix)

    def test_label_matrix_tc3(self):
        title_y = ['Name', 'Type', 'Description', 'Default', 'Bit']
        title_x = [
            'Term', 'Author', 'Description( B Is Lut Value)',
            'Default Value(32t)', 'Description( R Is Lut Value)',
            'Default Value(39t)', 'De Value', 'Comments',
            'Rev #', 'Date', 'Description( G Is Lut Value)',
            'Byte\\bit', 'Offset Address', 'Reg Addr',
            'Default Value', 'Address', 'Bit(s)', 'Def Value'
        ]
        title_matrix = self.label_matrix_tc(y=title_y, x=title_x, checker=lambda x, y, v: True)
        print(title_matrix)

    def test_label_matrix_tc4(self):
        title_y = [
            'Term', 'Author', 'Description( B Is Lut Value)',
            'Default Value(32t)', 'Description( R Is Lut Value)',
            'Default Value(39t)', 'De Value', 'Comments',
            'Rev #', 'Date', 'Description( G Is Lut Value)',
            'Byte\\bit', 'Offset Address', 'Reg Addr',
            'Default Value', 'Address', 'Bit(s)', 'Def Value'
        ]
        title_x = title_y[:]
        title_matrix = self.label_matrix_tc(y=title_y, x=title_x, checker=lambda x, y, v: True)
        print(title_matrix)

    def test_get_coordinates_of(self):
        title_y = [
            'Term', 'Author', 'Description( B Is Lut Value)',
            'Default Value(32t)', 'Description( R Is Lut Value)',
            'Default Value(39t)', 'De Value', 'Comments',
            'Rev #', 'Date', 'Description( G Is Lut Value)',
            'Byte\\bit', 'Offset Address', 'Reg Addr',
            'Default Value', 'Address', 'Bit(s)', 'Def Value'
        ]
        expect_coordinates = [('Def Value', 'De Value'),
                              ('Default Value(39t)', 'Default Value'),
                              ('Default Value(32t)', 'Default Value'),
                              ('Default Value(32t)', 'Default Value(39t)'),
                              ('Def Value', 'Default Value'),
                              ('Description( G Is Lut Value)', 'Description( R Is Lut Value)'),
                              ('Description( G Is Lut Value)', 'Description( B Is Lut Value)'),
                              ('Description( B Is Lut Value)', 'Description( R Is Lut Value)')]

        title_x = title_y[:]
        title_matrix = self.label_matrix_tc(y=title_y, x=title_x, checker=lambda x, y, v: True)
        coordinates = title_matrix.get_coordinates_of(less_and_equal_to=0.5, greater_than=0)
        # TODO: checker is needed, and less_than & greater_and_equal_to are needed to be tested
        # for coordinate in coordinates:
        print(coordinates)

    def test_is_valid_label_types(self):
        SimilarLabel.clear()
        all_stimu = ['Def Value', 'Default Value', 'Default Value(39t)', 'Default Value(32t)',
                     SimilarLabel.get('Default Value'),
                     (SimilarLabel.get('string1'), SimilarLabel.get('string2'), SimilarLabel.get('string3')),
                     (ColumnTitle.get('title1'), ColumnTitle.get('title2'))]
        wrong_stimu = [1, 1.0, SimilarLabelsGroups(), (1, 2, 3)]
        for stimu in all_stimu:
            if isinstance(stimu, tuple):
                self.assertTrue(SimilarLabelsGroups.is_valid_label_types(*stimu))
                print(','.join([str(s) for s in stimu]))
            else:
                self.assertTrue(SimilarLabelsGroups.is_valid_label_types(stimu))
                print(stimu if isinstance(stimu, str) else str(stimu))

        for stimu in wrong_stimu:
            if isinstance(stimu, tuple):
                self.assertRaises(TypeError, lambda: SimilarLabelsGroups.is_valid_label_types(*stimu))
            else:
                self.assertRaises(TypeError, lambda: SimilarLabelsGroups.is_valid_label_types(stimu))
            print('Exception Raised')
        SimilarLabel.clear()

    def test_append_new_label(self):
        all_stimu = ['Def Value', 'Default Value', 'Default Value(39t)', 'Default Value(32t)']
        expt = set(all_stimu)
        groups = SimilarLabelsGroups()
        for stimu in all_stimu:
            groups._append_new_label(stimu)
        print(expt)
        print(groups.labels)
        self.assertTrue(expt == groups.labels)

    def test_append_linked_labels(self):
        expt = [set(stimu) for stimu in self.one_group_tuple_stimu]

        groups = SimilarLabelsGroups()
        for stimu in self.one_group_tuple_stimu:
            groups._append_linked_labels(*stimu)
        for label in groups.all_label_names:
            for neighbor in label.neighbors:
                self.assertIn(set((str(label), str(neighbor))), expt)

    def test__graph_dfs(self):
        groups = SimilarLabelsGroups()
        for stimu in self.one_group_tuple_stimu:
            groups._append_linked_labels(*stimu)
        traversal = groups._graph_dfs(groups.all_label_names.pop(), set())
        print(traversal)

    def test__build_groups(self):
        expt = set(['Def Value', 'Default Value', 'Default Value(39t)', 'Default Value(32t)', 'De Value'])

        groups = SimilarLabelsGroups()
        for stimu in self.one_group_tuple_stimu:
            groups._append_linked_labels(*stimu)
        groups._build_groups()
        self.assertEqual(len(groups.groups), 1)
        self.assertEqual(set([str(label) for label in groups.groups[0]]), expt)

    def test_build_groups_from_tc0(self):
        expt0 = set(['Def Value', 'Default Value', 'Default Value(39t)', 'Default Value(32t)', 'De Value'])
        expt1 = set(['Description( G Is Lut Value)', 'Description( R Is Lut Value)', 'Description( B Is Lut Value)'])
        expt = expt0 | expt1

        groups = SimilarLabelsGroups()
        groups.build_groups_from(self.two_group_tuples_stimu)
        print('{0} groups are recoginzed, they are:'.format(len(groups.groups)))
        print([str(label) for label in groups.groups[0]])
        print([str(label) for label in groups.groups[1]])
        self.assertEqual(len(groups.groups), 2)
        obsv0 = set([str(label) for label in groups.groups[0]])
        obsv1 = set([str(label) for label in groups.groups[1]])
        obsv = obsv0 | obsv1
        self.assertEqual(obsv, expt)
        print(groups)

    def test_build_groups_from_tc1(self):
        stimu = ['Term', 'Comments', 'Date', 'Rev #', 'Author', 'Byte\\bit',
                 'Reg Addr', 'Offset Address', 'Address', 'Bit(s)']
        expt = set(stimu)

        groups = SimilarLabelsGroups()
        groups.build_groups_from(stimu)
        for i, group in enumerate(groups.groups):
            self.assertEqual(len(group), 1)
            self.assertIn(str(group[0]), expt)
            expt.discard(str(group[0]))
        self.assertEqual(len(expt), 0)

    def test_append_groups(self):
        stimu = ['Term', 'Comments', 'Date', 'Rev #', 'Author', 'Byte\\bit',
                 'Reg Addr', 'Offset Address', 'Address', 'Bit(s)']

        expt0 = set(['Def Value', 'Default Value', 'Default Value(39t)', 'Default Value(32t)', 'De Value'])
        expt1 = set(['Description( G Is Lut Value)', 'Description( R Is Lut Value)', 'Description( B Is Lut Value)'])
        expt = expt0 | expt1 | set(stimu)

        groups0 = SimilarLabelsGroups()
        groups0.build_groups_from(self.two_group_tuples_stimu)
        groups1 = SimilarLabelsGroups()
        groups1.build_groups_from(stimu)
        groups0.append_groups(groups1)

        groups_num = len(groups0.groups)
        print('{0} groups are recoginzed, they are:'.format(groups_num))
        obsv = set()
        for i in range(groups_num):
            print([str(label) for label in groups0.groups[i]])
            obsv |= set([str(label) for label in groups0.groups[i]])
        self.assertEqual(len(groups0.groups), 2+len(stimu))
        self.assertEqual(obsv, expt)

    def test_draw_similarity_from(self):
        stimu_tuple0 = [('Default', 'Default Value')]

        SimilarLabel.clear()
        groups0 = SimilarLabelsGroups()
        groups0.build_groups_from(stimu_tuple0)
        groups1 = SimilarLabelsGroups()
        groups1.build_groups_from(self.two_group_tuples_stimu)
        groups0.draw_similarity_from(groups1)
        print([str(label) for label in groups0.groups[0]])
        print([str(label) for label in groups1.groups[0]])
        self.assertEqual(len(groups0.groups), 1)
        self.assertEqual(len(groups1.groups), 1)

    def test_is_single_word(self):
        words = [
            'Abyssinian', 'Adelie Penguin', 'Affenpinscher', 'Afghan Hound', 'African Bush Elephant',
            'African Civet', 'African Clawed Frog', 'African Forest Elephant', 'African Palm Civet', 'African Penguin',
            'African Tree Toad', 'African Wild Dog', 'Ainu Dog', 'Airedale Terrier', 'Akbash',
            'Akita', 'Alaskan Malamute', 'Albatross', 'Aldabra Giant Tortoise', 'Alligator',
            'Alpine Dachsbracke', 'American Bulldog', 'American Cocker Spaniel', 'American Coonhound',
            'American Eskimo Dog', 'American Foxhound', 'American Pit Bull Terrier', 'American Staffordshire Terrier',
            'American Water Spaniel', 'Anatolian Shepherd Dog', 'Angelfish', 'Ant', 'Anteater',
            'Antelope', 'Appenzeller Dog', 'Arctic Fox', 'Arctic Hare', 'Arctic Wolf', 'Armadillo',
            'Asian Elephant', 'Asian Giant Hornet', 'Asian Palm Civet', 'Asiatic Black Bear',
            'Australian Cattle Dog',  'Australian Kelpie Dog', 'Australian Mist', 'Australian Shepherd',
            'Australian Terrier', 'Avocet', 'Axolotl', 'Aye Aye'
        ]
        expt_rslts = [
            True, False, True, False, False,
            False, False, False, False, False,
            False, False, False, False, True,
            True, False, True, False, True,
            False, False, False, False, False,
            False, False, False, False, False,
            True, True, True, True, False,
            False, False, False, True, False,
            False, False, False, False, False,
            False, False, False, True, True,
            False
        ]
        for i, word in enumerate(words):
            self.assertEqual(LabelSimilarityMatrix.is_single_word(word), expt_rslts[i])

    def test_is_in_wordnet(self):
        true_words = [
            'underlie', 'inherent', 'census', 'literacy', 'Pleistocene',
            'suffrage', 'intuitive', 'ideology', 'insurrection', 'empiricism',
            'incentive', 'bias', 'garrison', 'pancreas', 'vigilant',
            'mores', 'elicit', 'fixation', 'gradient', 'herbivore',
            'premise',  'contention', 'fauna', 'substrate'
        ]
        false_words = ['appendicularian', 'mmmm', 'notanEnglishWord']

        for word in true_words:
            try:
                self.assertTrue(LabelSimilarityMatrix.is_in_wordnet(word))
            except AssertionError as e:
                print('{0} is not in WordNet'.format(word))
                raise e

        for word in false_words:
            try:
                self.assertFalse(LabelSimilarityMatrix.is_in_wordnet(word))
            except AssertionError as e:
                print('{0} is in WordNet'.format(word))
                raise e
