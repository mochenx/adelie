# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.NLP.Label import LabelFrequency, LabelPartition


class UTLabelFrequency(unittest.TestCase):
    def setUp(self):
        with open('UT/UTsOfNLP/StimuForLabelFreq.txt', 'r') as f:
            all_words = f.read()
        self.words = all_words.split('\n')

    def check_sample_size(self, dut, expect_count, changed_val):
        self.assertEqual(dut.sample_size, expect_count)
        self.assertEqual(dut.sequence_changed, changed_val)
        print('sample_size: {0}'.format(dut.sample_size))

    def clear_n_new_dut(self, with_name):
        LabelFrequency.clear()
        dut = LabelFrequency.get(with_name)
        return dut

    def test_sample_size_expect_0(self):
        dut = self.clear_n_new_dut(with_name='UTsameple_size_expect_something')
        self.check_sample_size(dut, 0, True)

    def test_sample_size_expect_1(self):
        dut = self.clear_n_new_dut(with_name='UTsameple_size_expect_something')

        # Collect only ONE word
        dut.collect('Abandon')
        self.check_sample_size(dut, 1, False)

    def test_sample_size_expect_all(self):
        dut = self.clear_n_new_dut(with_name='UTsameple_size_expect_something')

        # Collect all  words read from StimuForLabelFreq.txt
        dut.collect(self.words)
        self.check_sample_size(dut, len(self.words), False)

    def test_get_partitions_empty(self):
        dut = self.clear_n_new_dut(with_name='UTget_partition_empty')
        empty_seq = False
        try:
            dut.get_partitions()
        except ValueError as e:
            empty_seq = True
        self.assertTrue(empty_seq)

    def test_get_partitions_test_max(self):
        dut = self.clear_n_new_dut(with_name='UTget_partition_test_max')

        # Collect all  words read from StimuForLabelFreq.txt
        dut.collect(self.words)
        partitions = dut.get_partitions()
        self.assertEqual(partitions.last_elem[0], 'able')
        self.assertEqual(partitions.last_elem[1], 13)
        print(partitions.last_elem)


class UTLabelPartition(unittest.TestCase):
    def setUp(self):
        with open('UT/UTsOfNLP/StimuForLabelFreq.txt', 'r') as f:
            all_words = f.read()
        self.words = all_words.split('\n')
        LabelFrequency.clear()
        self.freq = LabelFrequency.get('StimuForLabelFreq')
        self.freq.collect(self.words)

    def test_build_from_freq(self):
        stimu = [('Text1', 100), ('Text2', 95), ('Text3', 60), ('Text4', 50), ('Text5', 4), ('Text6', 4), ('Text7', 1)]
        expt = [set([('Text1', 100), ('Text2', 95)]),
                set([('Text3', 60), ('Text4', 50)]),
                set([('Text5', 4), ('Text6', 4), ('Text7', 1)])
        ]

        phrase_partitions = LabelPartition.build_from_freq(stimu)
        for partition in phrase_partitions:
            match = False
            for i, expt_partition in enumerate(expt[:]):
                if set(partition) == expt_partition:
                    expt.pop(i)
                    match = True
                    break
            if match:
                print('Partition Pass:{0}'.format(str(partition)))
            else:
                print('Partition Fail:{0}'.format(str(partition)))
        self.assertEqual(len(expt), 0)

    def test_of_freq_greater_exception(self):
        partitions = self.freq.get_partitions()
        self.assertRaises(ValueError, lambda: partitions.of_freq_greater())
        print('Exception has raised')

    def test_of_freq_greater_than(self):
        partitions = self.freq.get_partitions()
        selected_partitions = partitions.of_freq_greater(than=11)
        self.assertEqual(len(selected_partitions), 1)
        self.assertEqual(selected_partitions[0], ('able', 13))
        for partition in selected_partitions:
            print(partition)

    def test_of_freq_greater_and_equal_to(self):
        expt = set([('able', 13), ('free', 11), ('fruit', 11),
                    ('future', 11), ('friend', 11), ('front', 11),
                    ('from', 11), ('frequent', 11), ('full', 11)])
        partitions = self.freq.get_partitions()
        selected_partitions = partitions.of_freq_greater(and_equal_to=11)
        self.assertEqual(len(selected_partitions), 9)
        self.assertEqual(set(selected_partitions), expt)
        for partition in selected_partitions:
            print(partition)

    def test_of_freq_less_exception(self):
        partitions = self.freq.get_partitions()
        self.assertRaises(ValueError, lambda: partitions.of_freq_less())
        print('Exception has raised')

    def test_of_freq_less_than(self):
        partitions = self.freq.get_partitions()
        selected_partitions = partitions.of_freq_less(than=1)
        self.assertEqual(len(selected_partitions), 0)
        print('{0} words with frequency less than 1'.format(len(selected_partitions)))

    def test_of_freq_less_and_equal_to(self):
        partitions = self.freq.get_partitions()
        selected_partitions = partitions.of_freq_less(and_equal_to=1)
        print('{0} words with frequency 1'.format(len(selected_partitions)))
        self.assertGreater(len(selected_partitions), 0)
        for partition in selected_partitions:
            self.assertGreaterEqual(partition[1], 1)
