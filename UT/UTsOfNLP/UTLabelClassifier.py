# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.NLP.ColumnTitle import ColumnTitle
from RegDocxParser.NLP.LabelClassifier import SimilariryClassifier
from RegDocxParser.NLP.LabelSimilarity import LabelSimilarityMatrix, SimilarLabelsGroups
from RegDocxParser.Tokens.Category import Category


class UTSimilariryClassifier(unittest.TestCase):
    def setUp(self):
        self.majority_text = ['able', 'yes', 'yesterday', 'you', 'young']
        self.minority_text = 'abla,fly,fold,food,foolish,foot,for,force,fork,form,forward,fowl,frame,' \
                             'free,frequent,friend,from,front,fruit,full,future,garden,general'.split(',')
        self.majority_labels = [ColumnTitle.get(l) for l in self.majority_text]
        self.minority_labels = [ColumnTitle.get(l) for l in self.minority_text]
        self.majority = []
        self.all_labels_for_remain = ['suddenly', 'sugar',
                                      'suggestion', 'summer', 'sun', 'support', 'surprise', 'sweet', 'swim',
                                      'system', 'table', 'tail', 'take', 'talk', 'tall', 'taste', 'tax',
                                      'teaching', 'tendency', 'test', 'than', 'that', 'the', 'then']

        self.dut = SimilariryClassifier()
        self.dut.load_labels(self.majority_labels, self.minority_labels)

    @staticmethod
    def print_group(prefix, group):
        s = [str(label) for label in group.group_labels]
        print('{0} with size {2}\n\t{1}'.format(prefix, ','.join(s), len(s)))

    def print_dut(self):
        print(','.join([str(label) for label in self.dut.majority]))
        print(','.join([str(label) for label in self.dut.minority]))

    def test_major(self):
        self.assertEqual(self.dut.majority, self.majority_labels)
        self.print_dut()
        self.dut.majority = self.minority_labels
        self.assertEqual(self.dut.majority, self.minority_labels)
        self.print_dut()

    def test_major_type_err(self):
        def set_major():
            self.dut.majority = self.majority_text
        self.assertRaises(TypeError, set_major)
        print('TypeError is raised')

    def test_minor(self):
        self.assertEqual(self.dut.minority, self.minority_labels)
        self.print_dut()
        self.dut.minority = self.majority_labels
        self.assertEqual(self.dut.minority, self.majority_labels)
        self.print_dut()

    def test_minor_type_err(self):
        def set_minor():
            self.dut.minority = self.minority_text
        self.assertRaises(TypeError, set_minor)
        print('TypeError is raised')

    def test_get_similar_groups(self):
        grp_majority_n_minority = self.dut.get_similar_groups(self.dut.majority, self.dut.minority)
        self.assertEqual(set(grp_majority_n_minority.group_labels),
                         set(['able', 'abla']))
        self.print_group('Majority', grp_majority_n_minority)

    def test_similarity_match(self):
        grp_majority_n_minority, grp_minorities = self.dut.similarity_match()
        self.assertEqual(set(grp_majority_n_minority.group_labels),
                         set(['able', 'abla']))

        self.assertEqual(set(grp_minorities.group_labels),
                         set(self.minority_text) ^ set(['abla']))
        self.print_group('Majority', grp_majority_n_minority)
        self.print_group('Minority', grp_minorities)

    def check_test_of_get_remain_groups(self, group_num, func_cfg_stimu):
        groups = [SimilarLabelsGroups() for _ in range(group_num)]
        expt_group = func_cfg_stimu(groups)
        obsv_group = SimilariryClassifier.get_remain_groups(self.all_labels_for_remain, *groups)
        self.assertEqual(set(obsv_group.group_labels), set(expt_group))
        self.print_group('Remains', obsv_group)

    def test_get_remain_groups_not_in_1_group(self):
        def cfg_stimu(groups):
            groups[0].build_groups_from([ColumnTitle.get(l) for l in ['suddenly', 'sudden',
                                                                      'surprise', 'than', 'that']])
            expt_group = ['sugar', 'suggestion', 'summer', 'sun', 'support', 'sweet', 'swim',
                          'system', 'table', 'tail', 'take', 'talk', 'tall', 'taste', 'tax',
                          'teaching', 'tendency', 'test', 'the', 'then']
            return expt_group

        self.check_test_of_get_remain_groups(1, cfg_stimu)

    def test_get_remain_groups_not_in_2_groups(self):
        def cfg_stimu(groups):
            groups[0].build_groups_from([ColumnTitle.get(l) for l in ['suddenly', 'sudden']])
            groups[1].build_groups_from([ColumnTitle.get(l) for l in ['suger', 'sugar']])
            expt_group = ['suggestion', 'summer', 'sun', 'support', 'surprise', 'sweet', 'swim',
                          'system', 'table', 'tail', 'take', 'talk', 'tall', 'taste', 'tax',
                          'teaching', 'tendency', 'test', 'than', 'that', 'the', 'then']
            return expt_group

        self.check_test_of_get_remain_groups(2, cfg_stimu)

    def test_get_remain_groups_not_in_3_groups(self):
        def cfg_stimu(groups):
            groups[0].build_groups_from([ColumnTitle.get(l) for l in ['suddenly', 'sudden']])
            groups[1].build_groups_from([ColumnTitle.get(l) for l in ['sugar', 'sugar']])
            groups[2].build_groups_from([ColumnTitle.get(l) for l in ['t-a-x', 'tax', 't.ax']])
            expt_group = ['suggestion', 'summer', 'sun', 'support', 'surprise', 'sweet', 'swim',
                          'system', 'table', 'tail', 'take', 'talk', 'tall', 'taste',
                          'teaching', 'tendency', 'test', 'than', 'that', 'the', 'then']
            return expt_group
        self.check_test_of_get_remain_groups(3, cfg_stimu)

    def test_get_remain_groups_not_in_8_groups(self):
        def cfg_stimu(groups):
            for i, group in enumerate(groups):
                group.build_groups_from([l for l in [self.all_labels_for_remain[i]]])
            expt_group = ['swim', 'system', 'table', 'tail', 'take', 'talk', 'tall', 'taste', 'tax',
                          'teaching', 'tendency', 'test', 'than', 'that', 'the', 'then']
            return expt_group
        self.check_test_of_get_remain_groups(8, cfg_stimu)

    def check_test_of_refill_similar_categories(self, group_num, func_cfg_stimu):
        groups = [SimilarLabelsGroups() for _ in range(group_num)]
        expt_group = func_cfg_stimu(groups)
        self.dut.refill_similar_categories(*groups)
        for i in range(2):
            if expt_group[i] is None:
                continue
            for group in groups[i].groups:
                for label in group:
                    self.assertEqual(label.token, expt_group[i])
                    print('"{0}" with token "{1}"'.format(str(label), str(label.token)))
            self.print_group('Group', groups[i])

    def test_refill_similar_categories_major_only(self):
        def cfg_stimu(groups):
            label_list0 = [(ColumnTitle.get('suddenly'), ColumnTitle.get('sudden'))]
            label_list0[0][1].token = Category.get('sudden')
            label_list1 = [(ColumnTitle.get('suger'), ColumnTitle.get('sugar'))]
            groups[0].build_groups_from(label_list0)
            groups[1].build_groups_from(label_list1)
            # expt_group = [Category.get('sudden'), Category.get('sugar')]
            expt_group = [Category.get('sudden'), None]

            return expt_group
        self.check_test_of_refill_similar_categories(2, cfg_stimu)

    def test_refill_similar_categories(self):
        def cfg_stimu(groups):
            label_list0 = [(ColumnTitle.get('suddenly'), ColumnTitle.get('sudden'))]
            label_list0[0][1].token = Category.get('sudden')
            label_list1 = [(ColumnTitle.get('suger'), ColumnTitle.get('sugar'))]
            label_list1[0][1].token = Category.get('sugar')
            groups[0].build_groups_from(label_list0)
            groups[1].build_groups_from(label_list1)
            expt_group = [Category.get('sudden'), Category.get('sugar')]
            self.dut.preset_categories = {}
            self.dut.preset_categories['suger'] = 'sugar'

            return expt_group
        self.check_test_of_refill_similar_categories(2, cfg_stimu)
