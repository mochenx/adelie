# encoding = utf-8
__author__ = 'mochenx'

import unittest
from RegDocxParser.Tokens.Category import Category
from RegDocxParser.NLP.Phrase import SlicingInStream, PhraseTokenizer
from RegDocxParser.Stream.TextStream import TextStreamCursor, TextStream


class UTSlicingInStream(unittest.TestCase):
    def setUp(self):
        self.text = 'debt,decision,deep,degree,delicate,dependent,design,' \
                    'desire,destruction,detail,development,different,digestion,' \
                    'direction,dirty,discovery,discussion,disease,disgust,distance,distribution,division,' \
                    'do'
        self.stream = self.text.split(',')
        self.default_dut = SlicingInStream('discovery')

    def find_check(self, index, word, start_from=None):
        print('Finding {0}'.format(word))
        dut = SlicingInStream(word)
        expt_start = TextStreamCursor(index, 0)
        expt_end = TextStreamCursor(index, len(word))
        if start_from is None:
            start_from = TextStreamCursor(-1, -1)
        self.assertTrue(dut.find_word(self.stream, start_from))
        self.assertEqual(dut.start, expt_start)
        self.assertEqual(dut.end, expt_end)
        print('Find {0} at {1}.{2} ~ {1}.{3}'.format(word, dut.start.index, dut.start.position, dut.end.position))
        return dut.end

    def test_find_word_type_err(self):
        def call_find_word():
            self.default_dut.find_word(self.stream, 0)
        self.assertRaises(TypeError, call_find_word)

    def test_find_word(self):
        self.find_check(15, 'discovery')
        self.find_check(15, 'DISCovery')

    def test_find_half_word(self):
        self.find_check(4, 'deli')

    def test_find_des(self):
        start_from = TextStreamCursor(-1, -1)
        for idx in [6, 7, 8]:
            start_from = self.find_check(index=idx, word='des', start_from=start_from)

    def test_dont_find_word(self):
        print('Finding {0}'.format('discovery'))
        dut = SlicingInStream('discovery')
        start_from = TextStreamCursor(15, len('discovery'))
        self.assertFalse(dut.find_word(self.stream, start_from))
        start_from = TextStreamCursor(16, -1)
        self.assertFalse(dut.find_word(self.stream, start_from))
        start_from = TextStreamCursor(16, 0)
        self.assertFalse(dut.find_word(self.stream, start_from))


class UTPhraseTokenizer(unittest.TestCase):
    class FakeDocxText:
        def __init__(self, text=None):
            if text is None:
                self.text = 'in debt,decision,deep water,degree 1,delicate,dependent,10 design,' \
                            'desire,destruction,detail,development,different,digestion,' \
                            'direction,dirty,discovery,discussion,disease,disgust,distance,distribution,division,' \
                            'do my homework'
            else:
                self.text = text

    def setUp(self):
        self.text = 'in debt,decision,deep water,degree 1,delicate,dependent,10 design,' \
                    'desire,destruction,detail,development,different,digestion,' \
                    'direction,dirty,discovery,discussion,disease,disgust,distance,distribution,division,' \
                    'do my homework'
        self.stream = self.text.split(',')
        self.raw = UTPhraseTokenizer.FakeDocxText()
        self.text_stream = TextStream(raw_data=self.raw)
        self.text_stream.stream = self.stream

        self.phrases = [Category.get(name=('in', 'debt')),
                        Category.get(name=('deep', 'water')),
                        Category.get(name=('do', 'homework'))]
        self.expects = [[{'start': TextStreamCursor(0, 0), 'end': TextStreamCursor(0, 2)},
                         {'start': TextStreamCursor(0, 3), 'end': TextStreamCursor(0, 7)}],
                        [{'start': TextStreamCursor(2, 0), 'end': TextStreamCursor(2, 4)},
                         {'start': TextStreamCursor(2, 5), 'end': TextStreamCursor(2, 10)}],
                        [{'start': TextStreamCursor(22, 0), 'end': TextStreamCursor(22, 2)},
                         {'start': TextStreamCursor(22, 6), 'end': TextStreamCursor(22, 14)}]]

    def check_rslt_n_words(self, rslt, two_words, expect):
        the_1st_word, the_2nd_word = two_words
        self.assertTrue(rslt)
        self.assertEqual(the_1st_word.start, expect[0]['start'])
        self.assertEqual(the_1st_word.end, expect[0]['end'])
        self.assertEqual(the_2nd_word.start, expect[1]['start'])
        self.assertEqual(the_2nd_word.end, expect[1]['end'])
        print('"{0} {1}" checking passed'.format(the_1st_word.word, the_2nd_word.word))

    def test_find_phrase_in_stream_type_err(self):
        def find_phrase():
            PhraseTokenizer.find_phrase_in_stream(stream=self.stream, phrase='Some',
                                                  start_cursor=TextStreamCursor(-1, -1))
        self.assertRaises(TypeError, find_phrase)
        print('TypeError is raised')

    def test_find_phrase_in_stream(self):
        for i, phrase in enumerate(self.phrases):
            rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream, phrase=phrase,
                                                                    start_cursor=TextStreamCursor(-1, -1))
            self.check_rslt_n_words(rslt, two_words, self.expects[i])

    def test_find_phrase_in_stream_no_1st_word(self):
        rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream,
                                                                phrase=Category.get(('December', 'No')),
                                                                start_cursor=TextStreamCursor(-1, -1))
        self.assertFalse(rslt)
        self.assertIsNone(two_words)
        print('Can NOT find December in given stream')

    def test_find_phrase_in_stream_no_2nd_word(self):
        rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream,
                                                                phrase=Category.get(('decision', 'No')),
                                                                start_cursor=TextStreamCursor(-1, -1))
        self.assertFalse(rslt)
        self.assertIsNone(two_words)
        print('Can NOT find No in given stream')

    def test_find_phrase_in_stream_half_words(self):
        expects = [{'start': TextStreamCursor(1, 4), 'end': TextStreamCursor(1, 8)},
                   {'start': TextStreamCursor(3, 3), 'end': TextStreamCursor(3, 6)}]
        rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream,
                                                                phrase=Category.get(('sion', 'ree')),
                                                                start_cursor=TextStreamCursor(-1, -1))
        self.check_rslt_n_words(rslt, two_words, expects)

    def test_check_phrase_n_retry(self):
        for i, phrase in enumerate(self.phrases):
            rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream, phrase=phrase,
                                                                    start_cursor=TextStreamCursor(-1, -1))
            self.assertTrue(rslt)
            if i == 2:
                self.assertTrue(PhraseTokenizer.check_phrase_n_retry(the_two_words=two_words,
                                                                     stream=self.text_stream))
                print('"{0} {1}" checking passed'.format(two_words[0].word, two_words[1].word))
            else:
                self.assertFalse(PhraseTokenizer.check_phrase_n_retry(the_two_words=two_words,
                                                                      stream=self.text_stream))
                print('"{0} {1}" checking passed'.format(two_words[0].word, two_words[1].word))

    def test_rebuild_stream(self):
        expt = {0: 0, 1: 2}
        for i, phrase in enumerate(self.phrases):
            if i == 2:
                continue
            rslt, two_words = PhraseTokenizer.find_phrase_in_stream(stream=self.stream, phrase=phrase,
                                                                    start_cursor=TextStreamCursor(-1, -1))
            self.assertTrue(rslt)
            new_stream = PhraseTokenizer.rebuild_stream(the_two_words=two_words, stream=self.text_stream)
            self.assertEqual(new_stream[expt[i]].token, phrase)
            print('"{0} {1}" checking passed'.format(two_words[0].word, two_words[1].word))
            orig_str = ' '.join([s if isinstance(s, str) else str(s) for s in self.stream])
            new_str = ' '.join([s if isinstance(s, str) else str(s) for s in new_stream])
            self.assertEqual(orig_str, new_str)
            print('Original Stream: {0}'.format(orig_str))
            print('Rebuilt Stream: {0}'.format(new_str))
